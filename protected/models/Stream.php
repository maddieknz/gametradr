<?php

/**
 * This is the model class for table "stream".
 *
 * The followings are the available columns in table 'stream':
 * @property string $id
 * @property string $type
 * @property integer $visibility
 * @property string $user_id
 * @property string $context_class
 * @property integer $context_id
 * @property string $data
 * @property string $timestamp
 *
 * The followings are the available model relations:
 * @property User $user
 */
class Stream extends CActiveRecord
{
	const VISIBILITY_GLOBAL  = 1;
	const VISIBILITY_USER    = 2;
	const VISIBILITY_FRIENDS = 3;
	
	private $_context = false;

	public function setContext($context) {
		$this->_context = $context;
		$this->context_class = get_class($context);
		$this->context_id = $context->id;
	}
	
	public function getContext() {
		if($this->_context === false && $this->context_class) {
			$model = call_user_func($this->context_class.'::model');
			$this->_context = $model->findByPk($this->context_id);
		}
		return $this->_context;
	}
	
	private $_typeModel = false;
	
	public function getTypeModel() {
		if($this->_typeModel === false && $this->type) {
			$type = $this->type.'StreamMessage';
			Yii::import('application.models.streams.'.$type);
			$this->_typeModel = new $type;
			$this->_typeModel->init($this);
		}
		return $this->_typeModel;
	}
	
	public function getAgeText() {
		$span = time() - strtotime($this->timestamp);
		if($span < 60)
			return 'less than 1 minute ago';
		else if($span < 60 * 2)
			return '1 minute ago';
		else if($span < 60 * 60)
			return floor($span / 60) . ' minutes ago';
		else if($span < 60 * 60 * 2)
			return '1 hour ago';
		else if($span < 60 * 60 * 24)
			return floor($span / 60 / 60) . ' hours ago';
		else if($span < 60 * 60 * 24 * 2)
			return '1 day ago';
		else if($span < 60 * 60 * 24 * 365)
			return floor($span / 60 / 60 / 24) . ' days ago';
		else if($span < 60 * 60 * 24 * 365 * 2)
			return '1 year ago';
		else
			return floor($span / 60 / 60 / 24 / 365) . ' years ago';
	}
	
	public function isVisible($user = null) {
		return true; // TODO
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Stream the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'stream';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('type, visibility', 'required'),
			array('visibility, context_id', 'numerical', 'integerOnly'=>true),
			array('type, context_class', 'length', 'max'=>255),
			array('user_id', 'length', 'max'=>11),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, type, visibility, user_id, context_class, context_id, data, timestamp', 'safe', 'on'=>'search'),
		);
	}
	
	protected function afterFind()
	{		
		parent::afterFind();
		if($this->typeModel)
			$this->typeModel->afterFind();		
	}
	
	protected function beforeSave()
	{
		if(!parent::beforeSave())
			return false;
		if($this->typeModel)
			$this->typeModel->beforeSave();		
		return true;		
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'type' => 'Type',
			'visibility' => 'Visibility',
			'user_id' => 'User',
			'context_class' => 'Context Class',
			'context_id' => 'Context',
			'data' => 'Data',
			'timestamp' => 'Timestamp',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('visibility',$this->visibility);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('context_class',$this->context_class,true);
		$criteria->compare('context_id',$this->context_id);
		$criteria->compare('data',$this->data,true);
		$criteria->compare('timestamp',$this->timestamp,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}