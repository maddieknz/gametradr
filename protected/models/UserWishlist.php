<?php

/**
 * This is the model class for table "user_queue".
 *
 * The followings are the available columns in table 'user_queue':
 * @property string $id
 * @property string $user_id
 * @property integer $title_id
 * @property integer $priority
 * @property string $trade_id
 *
 * The followings are the available model relations:
 * @property Trade $trade
 * @property User $user
 * @property Title $title
 */
class UserWishlist extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return UserQueue the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_wishlist';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, title_id, priority', 'required'),
			array('title_id, priority', 'numerical', 'integerOnly'=>true),
			array('user_id, trade_id', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, title_id, priority, trade_id', 'safe', 'on'=>'search'),
		);
	}
	
	public function countCopies() {
		return count($this->getCopies());
	}
	
	private $_copies;
	
	public function getCopiesCriteria() {
		$c = new CDbCriteria;
		$c->with = 'title';
		$c->condition = 'title_id=:title';
		$c->params = array(':title'=>$this->title_id);
		// TODO restrict for visibility of titles
		return $c;
	}
	
	public function getCopies() {
		if($this->_copies == null) 			
			$this->_copies = Asset::model()->findAll($this->getCopiesCriteria());
		return $this->_copies;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'trade' => array(self::BELONGS_TO, 'AssetQueue', 'trade_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'title' => array(self::BELONGS_TO, 'Title', 'title_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'title_id' => 'Title',
			'priority' => 'Priority',
			'trade_id' => 'Trade',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('title_id',$this->title_id);
		$criteria->compare('priority',$this->priority);
		$criteria->compare('trade_id',$this->trade_id,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}