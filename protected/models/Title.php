<?php

/**
 * This is the model class for table "title".
 *
 * The followings are the available columns in table 'title':
 * @property integer $id
 * @property string $system_id
 * @property string $publisher
 * @property string $release_date
 * @property string $number_of_players
 * @property integer $status
 * @property string $submitter_id
 * @property string $submit_date
 * @property string $approve_date
 * @property string $reject_date
 * @property string $reject_comments
 *
 * The followings are the available model relations:
 * @property User $submitter
 * @property System $system
 */
class Title extends ActiveRecord
{
    const STATUS_SUBMITTED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_REJECTED = 2;       
    
	public $cover_picture = null; // For file upload
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return Title the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function findByUrlKey($url_key)
	{
		return $this->find('url_key=:key',array(':key'=>$url_key));
	}

	public function getViewUrl()
	{
		if($this->url_key)
		  return Yii::app()->createUrl('title/view',array('url_key'=>$this->url_key));
		else
		  return Yii::app()->createUrl('title/view',array('id'=>$this->id));
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'title';
	}
	
	public static function statusLabels() {
		return array(
			self::STATUS_SUBMITTED => 'Submitted',
			self::STATUS_ACTIVE => 'Active',
			self::STATUS_REJECTED => 'Rejected'
		);
	}

	public function getStatusLabel() {
		$labels = self::statusLabels();
		return isset($labels[$this->status]) ? $labels[$this->status] : 'n/a';
	}
	
	public function getCopiesDataProvider($user = null) {
	    $dataProvider=new CActiveDataProvider('Asset', array(
      		'criteria' => array(
				'condition'=>'title_id=:title',
				'params'=>array(':title'=>$this->id)
			)
        ));
		return $dataProvider;
	}

	
  public function behaviors()
  {
      return array(/*'datetimeI18NBehavior' => array('class' => 'ext.DateTimeI18NBehavior'),*/
       'CAdvancedArBehavior' => array( 'class' => 'ext.CAdvancedArBehavior')); 
  }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('release_date, number_of_players, status, name, description, system', 'required'),
			array('status, publisher_id, developer_id', 'numerical', 'integerOnly'=>true),
			array('system_id, submitter_id', 'length', 'max'=>11),
			array('publisher, name', 'length', 'max'=>255),
			array('number_of_players', 'length', 'max'=>10),
			array('url_key', 'unique'),
			array('genres', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, system_id, publisher_id, developer_id, release_date, number_of_players, status, submitter_id, submit_date, approve_date, reject_date, reject_comments', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'submitter' => array(self::BELONGS_TO, 'User', 'submitter_id'),
			'system' => array(self::BELONGS_TO, 'System', 'system_id'),
			'genres' => array(self::MANY_MANY, 'Genre', 'title_genre(title_id,genre_id)'),
			'publisher' => array(self::BELONGS_TO, 'Company', 'publisher_id'),
			'developer' => array(self::BELONGS_TO, 'Company', 'developer_id')
		);
	}

	protected function beforeValidate()
	{
		if($this->name && !$this->url_key)
		   $this->url_key = InflectorHelper::makeUrlKey($this->name);
		return parent::beforeValidate();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
		    'name' => 'Name',
		    'description' => 'Description',
			'system_id' => 'System',			
			'release_date' => 'Release Date',
			'number_of_players' => 'Number Of Players',
			'status' => 'Status',
			'publisher_id' => 'Publisher',
			'developer_id' => 'Developer',
			'submitter_id' => 'Submitter',
			'submit_date' => 'Submit Date',
			'approve_date' => 'Approve Date',
			'reject_date' => 'Reject Date',
			'reject_comments' => 'Reject Comments',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);		
		$criteria->compare('system_id',$this->system_id,true);
		$criteria->compare('publisher_id',$this->publisher_id,true);
		$criteria->compare('developer_id',$this->developer_id,true);
		$criteria->compare('release_date',$this->release_date,true);
		$criteria->compare('number_of_players',$this->number_of_players,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('submitter_id',$this->submitter_id,true);
		$criteria->compare('submit_date',$this->submit_date,true);
		$criteria->compare('approve_date',$this->approve_date,true);
		$criteria->compare('reject_date',$this->reject_date,true);
		$criteria->compare('reject_comments',$this->reject_comments,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
	
	protected function afterSave()
	{
	    if(($file=$this->cover_picture) instanceof CUploadedFile)		
	    {
		$media = new Media;
		$media->title_id = $this->id;
		$media->file_name = $file->name;
    	        $media->media_type = $file->type;
		$media->data = file_get_contents($file->tempName);
	        $media->status = Media::STATUS_DEFAULT;
		$media->type = Media::TYPE_TITLE_COVER;
	        $media->save();
	    }
            return parent::afterSave();
	}
}