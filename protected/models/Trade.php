<?php

/**
 * This is the model class for table "trade".
 *
 * The followings are the available columns in table 'trade':
 * @property string $id
 * @property integer $asset_id
 * @property string $user_id
 * @property integer $status
 * @property integer $last_trade_id
 * @property integer $next_trade_id
 * @property string $init_date
 * @property string $mail_date
 * @property string $receive_date
 * @property string $ready_date
 * @property string $finish_date
 *
 * The followings are the available model relations:
 * @property Asset $asset
 * @property User $user
 * @property UserQueue[] $userQueues
 */
class Trade extends CActiveRecord
{
    const STATUS_WAITING_FOR_MAILING    = 0;
    const STATUS_IN_TRANSIT             = 1;
    const STATUS_RECEIVED               = 2;
    const STATUS_READY_FOR_NEXT_MAILING = 3;
    const STATUS_FINISHED               = 4;
	/**
	 * Returns the static model of the specified AR class.
	 * @return Trade the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'trade';
	}
	
	public function findGamesToSend($user_id) {
	    return $this->findAll(
	      array(
	        'alias' => 'asset',
	        'condition' => 'status='.self::STATUS_WAITING_FOR_MAILING.' and last_user_id=:user_id',	        	
            'params' => array(':user_id'=>$user_id)));
	}

	public function findGamesToAck($user_id) {
	    return $this->findAll(
	      array(
	        'alias' => 'asset',
	        'condition' => 'status='.self::STATUS_IN_TRANSIT.' and user_id=:user_id',	        	
            'params' => array(':user_id'=>$user_id)));
	}	
	
	public function findGamesReceived($user_id) {
	  	return $this->findAll(
	      array(
	        'alias' => 'asset',
	        'condition' => 'status in ('.self::STATUS_RECEIVED.','.self::STATUS_READY_FOR_NEXT_MAILING.') and user_id=:user_id',	        	
            'params' => array(':user_id'=>$user_id)));
	}
	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('asset_id, user_id, status, init_date', 'required'),
			array('asset_id, status, last_trade_id, next_trade_id', 'numerical', 'integerOnly'=>true),
			array('user_id', 'length', 'max'=>11),
			array('mail_date, receive_date, ready_date, finish_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, asset_id, user_id, status, last_trade_id, next_trade_id, init_date, mail_date, receive_date, ready_date, finish_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'asset' => array(self::BELONGS_TO, 'Asset', 'asset_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'last_user' => array(self::BELONGS_TO, 'User', 'last_user_id'),		
			'userQueues' => array(self::HAS_MANY, 'UserQueue', 'trade_id'),
		    'next_trade' => array(self::HAS_ONE, 'Trade', 'next_trade_id'),
		    'last_trade' => array(self::HAS_ONE, 'Trade', 'last_trade_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'asset_id' => 'Asset',
			'user_id' => 'User',
			'status' => 'Status',
			'last_trade_id' => 'Last Trade',
			'next_trade_id' => 'Next Trade',
			'init_date' => 'Init Date',
			'mail_date' => 'Mail Date',
			'receive_date' => 'Receive Date',
			'ready_date' => 'Ready Date',
			'finish_date' => 'Finish Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('asset_id',$this->asset_id);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('last_trade_id',$this->last_trade_id);
		$criteria->compare('next_trade_id',$this->next_trade_id);
		$criteria->compare('init_date',$this->init_date,true);
		$criteria->compare('mail_date',$this->mail_date,true);
		$criteria->compare('receive_date',$this->receive_date,true);
		$criteria->compare('ready_date',$this->ready_date,true);
		$criteria->compare('finish_date',$this->finish_date,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}