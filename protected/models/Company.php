<?php

/**
 * This is the model class for table "company".
 *
 * The followings are the available columns in table 'company':
 * @property string $id
 * @property string $name
 * @property string $website_url
 *
 * The followings are the available model relations:
 * @property System[] $systems
 */
class Company extends CActiveRecord
{
	public $logo = null; // For file upload
	public $large_image = null; // For file upload
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Company the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public static function listData($condition = null) {
		return CHtml::listData(self::model()->findAll($condition),'id','name');
	}


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'company';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, website_url', 'required'),
			array('name, website_url', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, website_url', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'systems' => array(self::HAS_MANY, 'System', 'developer_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'website_url' => 'Website Url',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('website_url',$this->website_url,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	protected function afterSave()
	{
	    if(($file=$this->logo) instanceof CUploadedFile)		
	    {
		$media = new Media;
		$media->company_id = $this->id;
		$media->file_name = $file->name;
    	        $media->media_type = $file->type;
		$media->data = file_get_contents($file->tempName);
	        $media->status = Media::STATUS_DEFAULT;
		$media->type = Media::TYPE_COMPANY_LOGO;
	        $media->save();
	    }
	    if(($file=$this->large_image) instanceof CUploadedFile)		
	    {
		$media = new Media;
		$media->company_id = $this->id;
		$media->file_name = $file->name;
    	        $media->media_type = $file->type;
		$media->data = file_get_contents($file->tempName);
	        $media->status = Media::STATUS_DEFAULT;
		$media->type = Media::TYPE_LARGE_IMAGE;
	        $media->save();
	    }
            return parent::afterSave();
	}

}