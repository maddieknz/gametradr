<?php

/**
 * This is the model class for table "asset_history".
 *
 * The followings are the available columns in table 'asset_history':
 * @property string $id
 * @property integer $asset_id
 * @property string $queue_id
 * @property string $timestamp
 * @property integer $action
 * @property string $user_id
 * @property string $message
 *
 * The followings are the available model relations:
 * @property AssetQueue $queue
 * @property Asset $asset
 * @property User $user
 */
class AssetHistory extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AssetHistory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getStatusLabel() {
		return AssetQueue::getStatusLabel($this->action);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'asset_history';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('asset_id, action', 'required'),
			array('asset_id, action', 'numerical', 'integerOnly'=>true),
			array('queue_id, user_id', 'length', 'max'=>11),
			array('message', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, asset_id, queue_id, timestamp, action, user_id, message', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'queue' => array(self::BELONGS_TO, 'AssetQueue', 'queue_id'),
			'asset' => array(self::BELONGS_TO, 'Asset', 'asset_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'asset_id' => 'Asset',
			'queue_id' => 'Queue',
			'timestamp' => 'Timestamp',
			'action' => 'Action',
			'user_id' => 'User',
			'message' => 'Message',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('asset_id',$this->asset_id);
		$criteria->compare('queue_id',$this->queue_id,true);
		$criteria->compare('timestamp',$this->timestamp,true);
		$criteria->compare('action',$this->action);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('message',$this->message,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}