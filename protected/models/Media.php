<?php

/**
 * This is the model class for table "media".
 *
 * The followings are the available columns in table 'media':
 * @property string $id
 * @property integer $title_id
 * @property string $user_id
 * @property integer $company_id
 * @property string $file_name
 * @property string $mime_type
 * @property string $upload_date
 * @property integer $type
 * @property integer $status
 * @property string $upload_user_id
 * @property string $data
 * @property string $description
 * @property integer $sort_order
 *
 * The followings are the available model relations:
 * @property User $uploadUser
 * @property User $user
 * @property Title $title
 */
class Media extends CActiveRecord
{
	const TYPE_USER_PHOTO = 1;
	
	const TYPE_TITLE_COVER = 41;
	const TYPE_TITLE_BACK  = 42;
	const TYPE_TITLE_TITLE_SCREEN = 43;
	const TYPE_TITLE_SCREENSHOT = 44;

	const TYPE_SYSTEM_IMAGE = 81;

	const TYPE_COMPANY_LOGO = 101;
	const TYPE_COMPANY_LARGE_IMAGE = 102;
	
	const STATUS_PENDING = 0;
	const STATUS_ENABLED = 1;
	const STATUS_DEFAULT = 2;
	const STATUS_BLOCKED = 3;
	const STATUS_DISABLED = 4;
	
	public $file = null; // New uploads

	public static function types() {
		return array(
			array('type'=>self::TYPE_USER_PHOTO,'model'=>'User','label'=>'User:Photo'),
			array('type'=>self::TYPE_TITLE_COVER,'model'=>'Title','label'=>'Title:Cover'),
			array('type'=>self::TYPE_TITLE_BACK,'model'=>'Title','label'=>'Title:Back'),
			array('type'=>self::TYPE_TITLE_TITLE_SCREEN,'model'=>'Title','label'=>'Title:Title Screen'),
			array('type'=>self::TYPE_TITLE_SCREENSHOT,'model'=>'Title','label'=>'Title:Screenshot'),
			array('type'=>self::TYPE_SYSTEM_IMAGE,'model'=>'System','label'=>'System:Image'),			
			array('type'=>self::TYPE_COMPANY_LOGO,'model'=>'Company','label'=>'Company:Logo'),			
			array('type'=>self::TYPE_COMPANY_LARGE_IMAGE,'model'=>'Company','label'=>'Company:Large Image'),			
		);
	}
	
	public static function typeLabels() {
		return CHtml::listData(self::types(),'type','label');
	}
	
	public static function statusLabels() {
		return array(
			self::STATUS_PENDING => 'Pending',
			self::STATUS_ENABLED => 'Enabled',
			self::STATUS_DEFAULT => 'Default',
			self::STATUS_BLOCKED => 'Blocked',
			self::STATUS_DISABLED => 'Disabled'
		);
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Media the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'media';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('file_name, media_type, type, status', 'required'),
			array('title_id, system_id, user_id, company_id, upload_user_id, type, status, sort_order', 'numerical', 'integerOnly'=>true),
			array('file_name, media_type, media_url', 'length', 'max'=>255),
			array('description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title_id, user_id, file_name, media_type, upload_date, type, status, upload_user_id, description, sort_order', 'safe', 'on'=>'search'),
		);
	}

	protected function beforeValidate() {
		  if($this->scenario == 'insert' && !$this->upload_user_id)
		  	$this->upload_user_id = Yii::app()->user->id;

		if(($file=$this->file) instanceof CUploadedFile) {
			$this->file_name = $file->name;
			$this->media_type = $file->type;
			$this->data = file_get_contents($file->tempName);
	        }
		if(!$this->user_id) $this->user_id = null;
		
		  return parent::beforeValidate();
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'uploadUser' => array(self::BELONGS_TO, 'User', 'upload_user_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'title' => array(self::BELONGS_TO, 'Title', 'title_id'),
     		   	'system' => array(self::BELONGS_TO, 'System', 'system_id'),
     		   	'company' => array(self::BELONGS_TO, 'Company', 'company_id'),			
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title_id' => 'Title',
			'user_id' => 'User',
			'file_name' => 'File Name',
			'media_type' => 'Media Type',
			'system_id' => 'System',
			'company_id' => 'Company',
			'upload_date' => 'Upload Date',
			'type' => 'Type',
			'status' => 'Status',
			'upload_user_id' => 'Upload User',
			'data' => 'Data',
			'description' => 'Description',
			'sort_order' => 'Sort Order',
			'media_url' => 'Media URL'
		);
	}

	public function sendToBrowser() 
	{
	  Yii::app()->visitRecorder->is_page_view = false;		
	  header('Pragma: public');
      	  header('Expires: 0');
      	  header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	  header('Content-Length: '.strlen($this->data));
      	  header('Content-Type: '.$this->media_type);
	  header('Content-Disposition: inline; filename='.$this->file_name);
	  echo $this->data;	  
	  die();
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('title_id',$this->title_id,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('system_id',$this->system_id,true);
		$criteria->compare('company_id',$this->company_id,true);		
		$criteria->compare('file_name',$this->file_name,true);
		$criteria->compare('media_type',$this->media_type,true);
		$criteria->compare('upload_date',$this->upload_date,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('status',$this->status);
		$criteria->compare('upload_user_id',$this->upload_user_id,true);
		$criteria->compare('data',$this->data,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('sort_order',$this->sort_order);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}