<?php

/**
 * This is the model class for table "asset".
 *
 * The followings are the available columns in table 'asset':
 * @property integer $id
 * @property integer $title_id
 * @property string $owner_id
 * @property integer $available
 * @property integer $trade_policy
 * @property integer $visibility
 *
 * The followings are the available model relations:
 * @property User $owner
 * @property Title $title
 * @property AssetHistory[] $history
 * @property AssetQueue[] $queue 
 */
class Asset extends CActiveRecord
{
    const VISIBILITY_DEFAULT = 0;
    const VISIBILITY_JUST_ME = 1;
    const VISIBILITY_FRIENDS_ONLY = 2;
    const VISIBILITY_ALL_USERS =3;
    
    const TRADE_POLICY_DEFAULT = 0;
    const TRADE_POLICY_NOBODY = 1;
    const TRADE_POLICY_FRIENDS_ONLY = 2;
    const TRADE_POLICY_ALL_USERS = 3;
    
    public static function visibilityOptions() {
       return array(
         self::VISIBILITY_DEFAULT=>'Default',
         self::VISIBILITY_JUST_ME=>'Just Me',
         self::VISIBILITY_FRIENDS_ONLY=>'Friends Only',
         self::VISIBILITY_ALL_USERS=>'All Users'
       );
    }
    
    public static function tradeOptions() {
       return array(
         self::TRADE_POLICY_DEFAULT=>'Default',
         self::TRADE_POLICY_NOBODY=>'Nobody',
         self::TRADE_POLICY_FRIENDS_ONLY=>'With Friends Only',
         self::TRADE_POLICY_ALL_USERS=>'With All Users'
       );      
    }
    
    public static function visibilityOption($value) {
       $options = self::visibilityOptions();
       return isset($options[$value]) ? $options[$value] : '';
    }

    public static function tradeOption($value) {
       $options = self::tradeOptions();
       return isset($options[$value]) ? $options[$value] : '';
    }

    public function updateHistory() {

       // Find the last history item
       $queue = $this->getRelated('queue',true);
       $history = $this->getRelated('lastHistory',true);
	   
	    // No history case, use the first item in the queue
	    if(!$history)
			$idx = 0;
	    // If the last history item has no queue_id, then we returned the game to the owner	   
		else if(!$history->queue_id) {
			if($history->action != AssetQueue::STATUS_COMPLETE) return; // Do nothing until received by owner
			
			// Find the next queue item in pending status
			for($idx = 0; $idx < count($queue); $idx++)
				if($queue[$idx]->status == AssetQueue::STATUS_PENDING)
					break;
		} else	   
	       $idx = array_search($history->queue,$queue);

       if($idx === false || !isset($queue[$idx]))
          return; // Invalid pointer or no queue

       $current = $queue[$idx];

		// If the current status is complete, trigger the next queue item, if none, return to sender
	    if($current->status == AssetQueue::STATUS_COMPLETE) {
			if($idx == count($queue)-1) {
				// Queue is empty, start a return to owner
				$hx = new AssetHistory;
				$hx->asset_id = $this->id;
				$hx->user_id = $current->user_id;
				$hx->action = AssetQueue::STATUS_WAITING_FOR_SHIP;
				$hx->save();
			} else {
				$idx++;
			    $current = $queue[$idx];
			}			
		}
       
       // If the current status is pending, put in "waiting to ship" status
       if($current->status == AssetQueue::STATUS_PENDING)
          $this->updateStatus(AssetQueue::STATUS_WAITING_FOR_SHIP,$current);
		  
    }
		
	public function updateStatus($newStatus,$queue = null) {
		$tx = Yii::app()->db->beginTransaction();
		$history = $this->getRelated('lastHistory',true);
		if(!$queue)
			$queue = $history ? $history->queue : null;
		if($queue) {
			$queue->status = $newStatus;
			if(!$queue->save()) {
			  $tx->rollBack();
			  return false;
			}
		}
		
		$hx = new AssetHistory;
		$hx->asset_id = $this->id;
		$hx->queue_id = $queue ? $queue->id : null;

		if($newStatus == AssetQueue::STATUS_WAITING_FOR_SHIP)
		 $hx->user_id = $this->getCurrentUser()->id;
		else
		 $hx->user_id = $queue ? $queue->user_id : $this->owner_id;

		$hx->action = $newStatus;
		if(!$hx->save()) {
		  $tx->rollBack();
		  return false;
		}
		$tx->commit();
		return true;
		
	}    
    
  	/**
	 * Returns the static model of the specified AR class.
	 * @return Asset the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'asset';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title_id, owner_id, available, trade_policy, visibility', 'required'),
			array('title_id, available, trade_policy, visibility', 'numerical', 'integerOnly'=>true),
			array('owner_id', 'length', 'max'=>11),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title_id, owner_id, available, trade_policy, visibility', 'safe', 'on'=>'search'),
		);
	}

	public function getCurrentUser() {

	       return $this->lastPlayed ? $this->lastPlayed->user
	       	      : $this->owner;
	}
	
	public function isVisible($user = null) {
		return true; // TODO visibility check
	}
	
	public static function applyVisibilityCriteriaForLibrary($libraryOwner,$criteria) {
		
		// Can always see own library
		if($libraryOwner->id == Yii::app()->user->id)
			return;

		$default_vis = $libraryOwner->default_visibility;
		$is_friend = $libraryOwner->isFriend(Yii::app()->user->id);
		
		$values = array(self::VISIBILITY_ALL_USERS);
		
		if($is_friend)
			$values[] = self::VISIBILITY_FRIENDS_ONLY;
		
		if($default_vis == self::VISIBILITY_FRIENDS_ONLY and $is_friend)
			$values[] = self::VISIBILITY_DEFAULT;
		
		if($default_vis == self::VISIBILITY_ALL_USERS)
			$values[] = self::VISIBILITY_DEFAULT;
		
		$criteria->addCondition('visibility in ('.implode(',',$values).')');
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'owner' => array(self::BELONGS_TO, 'User', 'owner_id'),
			'title' => array(self::BELONGS_TO, 'Title', 'title_id'),
			'history' => array(self::HAS_MANY, 'AssetHistory', 'asset_id'),
			'lastHistory' => array(self::HAS_ONE, 'AssetHistory', 'asset_id', 'condition' =>
			   'not exists (select id from asset_history h2 where h2.asset_id=lastHistory.asset_id '.
			   'and h2.id>lastHistory.id)'),
			'lastPlayed' => array(self::HAS_ONE, 'AssetHistory', 'asset_id', 'condition' =>
			   'not exists (select id from asset_history h2 where h2.asset_id=lastPlayed.asset_id '.
			   'and h2.id>lastPlayed.id) and action=:status',
			   'params'=>array(':status'=>AssetQueue::STATUS_PLAYING)),
			'queue' => array(self::HAS_MANY, 'AssetQueue', 'asset_id','order'=>'seq_num'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title_id' => 'Title',
			'owner_id' => 'Owner',
			'available' => 'Available',
			'trade_policy' => 'Trade Policy',
			'visibility' => 'Visibility',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title_id',$this->title_id);
		$criteria->compare('owner_id',$this->owner_id,true);
		$criteria->compare('available',$this->available);
		$criteria->compare('trade_policy',$this->trade_policy);
		$criteria->compare('visibility',$this->visibility);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}