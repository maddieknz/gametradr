<?php

/**
 * This is the model class for table "system".
 *
 * The followings are the available columns in table 'system':
 * @property string $id
 * @property string $name
 * @property string $developer_id
 * @property string $release_date
 *
 * The followings are the available model relations:
 * @property Media[] $medias
 * @property Company $developer
 * @property Title[] $titles
 */
class System extends CActiveRecord
{
	public $picture = null; // For file upload

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return System the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function findByUrlKey($url_key)
	{
		return $this->find('url_key=:key',array(':key'=>$url_key));
	}

	
	public static function listData($condition = null) {
		return CHtml::listData(self::model()->findAll($condition),'id','name');
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'system';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, developer_id, release_date', 'required'),
			array('featured_sort_order', 'numerical', 'integerOnly' => true),
			array('name', 'length', 'max'=>255),
			array('developer_id', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, developer_id, release_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'media' => array(self::HAS_MANY, 'Media', 'system_id'),
			'developer' => array(self::BELONGS_TO, 'Company', 'developer_id'),
			'titles' => array(self::HAS_MANY, 'Title', 'system_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'developer_id' => 'Developer',
			'release_date' => 'Release Date',
			'featured_sort_order' => 'Featured Sort Order'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('developer_id',$this->developer_id,true);
		$criteria->compare('release_date',$this->release_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

/*  public function behaviors()
  {
      return array('datetimeI18NBehavior' => array('class' => 'ext.DateTimeI18NBehavior')); 
  }*/

	protected function beforeValidate()
	{
		if($this->name && !$this->url_key)
		   $this->url_key = InflectorHelper::makeUrlKey($this->name);
		return parent::beforeValidate();
	}
  
    protected function afterSave()
    {        
        if(($file=$this->picture) instanceof CUploadedFile) {
	    $media = new Media;
	        $media->system_id = $this->id;
		    $media->file_name = $file->name;
		        $media->media_type = $file->type;
            $media->data = file_get_contents($file->tempName);
	        $media->status = Media::STATUS_DEFAULT;
		    $media->type = Media::TYPE_SYSTEM_IMAGE;
		        if(!$media->save()) {
			      var_dump($media->errors);
			            die();
				        }	    
        }
        
        return parent::afterSave();
    }
}