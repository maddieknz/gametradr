<?php

abstract class IStreamMessage extends CModel {
  
  private static $_names=array();
  
  protected $stream = null;
  
  public function init($stream) {
    $this->stream = $stream;    
  }
  
  public function attributeNames()
  {
    $className=get_class($this);
    if(!isset(self::$_names[$className]))
    {
        $class=new ReflectionClass(get_class($this));
        $names=array();
        foreach($class->getProperties() as $property)
        {
            $name=$property->getName();
            if($property->isPublic() && !$property->isStatic())
                $names[]=$name;
        }
        return self::$_names[$className]=$names;
    }
    else
        return self::$_names[$className];
  }
  
  public abstract function renderMessage();
  
  public function getFullPageUrl() {
    return Yii::app()->createUrl('stream/view',array('id'=>$this->stream->id));
  }
  
  public function renderFullPage() { return false; }
  
  public function afterFind() {
    $this->setAttributes(CJSON::decode($this->stream->data),false);
  }
  
  public function beforeSave() {
    $this->stream->data = CJSON::encode($this->attributes);    
  }
}

?>