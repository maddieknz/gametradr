<?php

/**
 * This is the model class for table "user_invite".
 *
 * The followings are the available columns in table 'user_invite':
 * @property integer $id
 * @property integer $user_id
 * @property string $random
 */
class UserInvite extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return UserInvite the static model class
	 */
	
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function generateRandom(){
		$this->random = rand(23456789,98765432);
		
		
		
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_invite';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, random', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('random', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, random', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'user_id' => 'User',
			'random' => 'Random',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('random',$this->random,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}