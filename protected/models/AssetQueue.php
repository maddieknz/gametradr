<?php

/**
 * This is the model class for table "asset_queue".
 *
 * The followings are the available columns in table 'asset_queue':
 * @property string $id
 * @property integer $asset_id
 * @property integer $seq_num
 * @property string $user_id
 * @property integer $status
 * @property integer $duration
 * @property string $requested_date
 *
 * The followings are the available model relations:
 * @property AssetHistory[] $assetHistories
 * @property Asset $asset
 * @property User $user
 * @property UserWishlist[] $userWishlists
 */
class AssetQueue extends CActiveRecord
{
	const STATUS_PENDING          = 0;
	const STATUS_HOLD             = 1;
	const STATUS_WAITING_FOR_SHIP = 2;
	const STATUS_IN_TRANSIT       = 3;
	const STATUS_PLAYING          = 4;
	const STATUS_COMPLETE         = 5;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AssetQueue the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getGenericStatus()
	{
		return $this->getUserStatus();
	}
	
	public function getUserStatus()
	{
		return self::getStatusLabel($this->status);
	}
	
	public static function getStatusLabel($status) {
		switch($status) {
			case self::STATUS_PENDING: return 'Pending';
			case self::STATUS_HOLD: return 'On Hold';
			case self::STATUS_WAITING_FOR_SHIP: return 'Waiting for Ship';
			case self::STATUS_IN_TRANSIT: return 'In Transit';
			case self::STATUS_PLAYING: return 'Playing';
			case self::STATUS_COMPLETE: return 'Complete';				
		}		
	}
	
	public function getUserActionUrl()
	{
		if($this->status == self::STATUS_PLAYING)
			return Yii::app()->createUrl('asset/complete',array('id'=>$this->asset_id));
		return false;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'asset_queue';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('asset_id, seq_num, user_id, status, duration, requested_date', 'required'),
			array('asset_id, seq_num, status, duration', 'numerical', 'integerOnly'=>true),
			array('user_id', 'length', 'max'=>11),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, asset_id, seq_num, user_id, status, duration, requested_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'assetHistories' => array(self::HAS_MANY, 'AssetHistory', 'queue_id'),
			'asset' => array(self::BELONGS_TO, 'Asset', 'asset_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'userWishlists' => array(self::HAS_MANY, 'UserWishlist', 'trade_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'asset_id' => 'Asset',
			'seq_num' => 'Seq Num',
			'user_id' => 'User',
			'status' => 'Status',
			'duration' => 'Duration',
			'requested_date' => 'Requested Date',
		);
	}


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('asset_id',$this->asset_id);
		$criteria->compare('seq_num',$this->seq_num);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('duration',$this->duration);
		$criteria->compare('requested_date',$this->requested_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}