<?php

/**
 * This is the model class for table "asset_request".
 *
 * The followings are the available columns in table 'asset_request':
 * @property integer $id
 * @property integer $asset_id
 * @property string $user_id
 * @property integer $status
 * @property string $request_date
 * @property integer $duration
 * @property string $comment
 *
 * The followings are the available model relations:
 * @property Asset $asset
 * @property User $user
 */
class AssetRequest extends CActiveRecord
{
	const STATUS_REQUESTED = 0;
	const STATUS_ACCEPTED = 1;
	const STATUS_REJECTED = 2;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AssetRequest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'asset_request';
	}
	
	public function convertToQueue()
	{
		$model = new AssetQueue;
		$model->asset_id = $this->asset_id;
		$model->user_id = $this->user_id;
		$model->status = 0; // Pending
		$model->duration = $this->duration;
		$model->requested_date = $this->request_date;
		
		// Find the max seq_num in the queue
		$seq_num = Yii::app()->db->createCommand('select max(seq_num) from asset_queue where asset_id=:asset')->queryScalar(array(':asset'=>$this->asset_id));
		$model->seq_num = $seq_num+1;
		return $model->save() ? $model : null;		
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('asset_id, user_id, status, request_date, duration', 'required'),
			array('id, asset_id, status, duration', 'numerical', 'integerOnly'=>true),
			array('user_id', 'length', 'max'=>11),
			array('comment', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, asset_id, user_id, status, request_date, duration, comment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'asset' => array(self::BELONGS_TO, 'Asset', 'asset_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'asset_id' => 'Asset',
			'user_id' => 'User',
			'status' => 'Status',
			'request_date' => 'Request Date',
			'duration' => 'Duration',
			'comment' => 'Comment',
		);
	}

	protected function afterSave() {

		  // If approved, update asset history, possibly triggering the trade process
		  if($this->status == self::STATUS_ACCEPTED)
		    $this->asset->updateHistory();

		  parent::afterSave();
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('asset_id',$this->asset_id);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('request_date',$this->request_date,true);
		$criteria->compare('duration',$this->duration);
		$criteria->compare('comment',$this->comment,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}