<?php

class TitleIndexFilter extends CFormModel {

      public $system_id;
      public $genre_id;

      public function rules() {
        return array(
	  array('system_id,genre_id','numerical','integerOnly'=>true));
      }

      public function attributeLabels() {
         return array(
	    'system_id'=>'System',
	    'genre_id'=>'Genre'
	 );
      }

      public function apply($criteria) {
         $criteria->compare('system_id',$this->system_id);
	 if($this->genre_id) {
	    $criteria->with = 'genres';
	    $criteria->together = true;
/*	     if(!is_array($criteria->with)) 
	 	$criteria->with = array('genres');
	     else if(!in_array('genres',$criteria->with))
	        $critiera->with[] = 'genres';*/
             $criteria->compare('genres.id',$this->genre_id);
	 }
      }
}
?>