<?php

/**
 * This is the model class for table "visit_online".
 *
 * The followings are the available columns in table 'visit_online':
 * @property string $visit_id
 * @property string $visitor_id
 * @property string $user_id
 * @property string $visit_start_at
 * @property string $last_page_at
 * @property string $last_page
 * @property integer $pageviews
 *
 * The followings are the available model relations:
 * @property Visitor $visitor
 * @property Visit $visit
 * @property User $user
 */
class VisitOnline extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return VisitOnline the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'visit_online';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('visit_id, visitor_id, visit_start_at, last_page_at, last_page, pageviews', 'required'),
			array('pageviews', 'numerical', 'integerOnly'=>true),
			array('visit_id, visitor_id', 'length', 'max'=>10),
			array('user_id', 'length', 'max'=>11),
			array('last_page', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('visit_id, visitor_id, user_id, visit_start_at, last_page_at, last_page, pageviews', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'visitor' => array(self::BELONGS_TO, 'Visitor', 'visitor_id'),
			'visit' => array(self::BELONGS_TO, 'Visit', 'visit_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'visit_id' => 'Visit',
			'visitor_id' => 'Visitor',
			'user_id' => 'User',
			'visit_start_at' => 'Visit Start At',
			'last_page_at' => 'Last Page At',
			'last_page' => 'Last Page',
			'pageviews' => 'Pageviews',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('visit_id',$this->visit_id,true);
		$criteria->compare('visitor_id',$this->visitor_id,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('visit_start_at',$this->visit_start_at,true);
		$criteria->compare('last_page_at',$this->last_page_at,true);
		$criteria->compare('last_page',$this->last_page,true);
		$criteria->compare('pageviews',$this->pageviews);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}