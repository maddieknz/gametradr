<?php

/**
 * This is the model class for table "visit".
 *
 * The followings are the available columns in table 'visit':
 * @property string $id
 * @property string $visitor_id
 * @property integer $type
 * @property string $http_referer
 * @property string $ip_address
 * @property string $user_agent
 * @property string $browser
 * @property string $operating_system
 * @property string $screen_resolution
 * @property string $landing_page
 * @property string $visit_start_at
 * @property string $last_page
 * @property string $last_page_at
 * @property string $language
 * @property integer $pageviews
 *
 * The followings are the available model relations:
 * @property VisitOnline $visitOnline
 */
class Visit extends CActiveRecord
{
	const TYPE_NEW = 1;
	const TYPE_RETURNING = 2;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Visit the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'visit';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('visitor_id, type, http_referer, ip_address, user_agent, landing_page, visit_start_at', 'required'),
			array('type, pageviews', 'numerical', 'integerOnly'=>true),
			array('visitor_id', 'length', 'max'=>10),
			array('http_referer, ip_address, user_agent, browser, operating_system, screen_resolution, landing_page, last_page, language', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, visitor_id, type, http_referer, ip_address, user_agent, browser, operating_system, screen_resolution, landing_page, visit_start_at, last_page, last_page_at, language, pageviews', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'visitor' => array(self::BELONGS_TO, 'Visitor', 'visitor_id'),
			'visitOnline' => array(self::HAS_ONE, 'VisitOnline', 'visit_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'visitor_id' => 'Visitor',
			'type' => 'Type',
			'http_referer' => 'Http Referer',
			'ip_address' => 'Ip Address',
			'user_agent' => 'User Agent',
			'browser' => 'Browser',
			'operating_system' => 'Operating System',
			'screen_resolution' => 'Screen Resolution',
			'landing_page' => 'Landing Page',
			'visit_start_at' => 'Visit Start At',
			'last_page' => 'Last Page',
			'last_page_at' => 'Last Page At',
			'language' => 'Language',
			'pageviews' => 'Pageviews',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('visitor_id',$this->visitor_id,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('http_referer',$this->http_referer,true);
		$criteria->compare('ip_address',$this->ip_address,true);
		$criteria->compare('user_agent',$this->user_agent,true);
		$criteria->compare('browser',$this->browser,true);
		$criteria->compare('operating_system',$this->operating_system,true);
		$criteria->compare('screen_resolution',$this->screen_resolution,true);
		$criteria->compare('landing_page',$this->landing_page,true);
		$criteria->compare('visit_start_at',$this->visit_start_at,true);
		$criteria->compare('last_page',$this->last_page,true);
		$criteria->compare('last_page_at',$this->last_page_at,true);
		$criteria->compare('language',$this->language,true);
		$criteria->compare('pageviews',$this->pageviews);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}