<?php

class NewFriendsStreamMessage extends IStreamMessage {
  
  public $user1;
  public $user2;
    
  public function renderMessage() {

    if($this->user1 == Yii::app()->user->id)
        return 'You have become friends with '.UserHelper::userLink(User::model()->findByPk($this->user2)) . '.';
    if($this->user2 == Yii::app()->user->id)
        return 'You have become friends with '.UserHelper::userLink(User::model()->findByPk($this->user1)) . '.';
    
    $links = array();
    foreach(User::model()->findAll('id in (:user1,:user2)',array(
        ':user1'=>$this->user1,':user2'=>$this->user2)) as $user)
        $links[] =  UserHelper::userLink($user);

    $html = implode(' and ',$links) . ' have become friends.';
    return $html;
  }  
}

?>