<?php

class NewTitleStreamMessage extends IStreamMessage {
  
  public $titleIds = array();
  private $_titles = false;
  
  public function addTitle($title) {
    $this->titleIds[] = is_object($title) ? $title->id : (int) $title;
    if($this->_titles)
        $this->_titles[] = $title;
  }
  
  public function getTitles() {
    if($this->_titles === false)
        $this->_titles = Title::model()->findAll('id in ('.implode(',',$this->titleIds).')');
    return $this->_titles;
  }
  
  public function renderMessage() {
    $titles = $this->titles;
    if(empty($titles))
        return false;
    
    $html = UserHelper::userLink($this->stream->user) . ' added ';
    if(count($titles) == 1)
        $html .= TitleHelper::titleLink($titles[0]);
    else if(count($titles) == 2)
        $html .= TitleHelper::titleLink($titles[0]) . ' and ' . TitleHelper::titleLink($titles[1]);
    else
        $html .= CHTML::link(count($titles) . ' titles',
                             $this->getFullPageUrl());
    $html .= ' to '.Yii::app()->name.'.';
    return $html;
  }
  
  public function renderFullPage() {
    $html = UserHelper::userLink($this->stream->user) . ' added the following titles:<ul class="titles">';
    foreach($this->titles as $title)
      $html .= '<li>' . TitleHelper::titleLink($title) . '</li>';
    $html .= '</ul>';
    return $html;   
  }
}

?>