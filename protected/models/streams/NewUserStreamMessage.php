<?php

class NewUserStreamMessage extends IStreamMessage {
  
  public function renderMessage() {
    return UserHelper::userLink($this->stream->user) . ' has joined ' . Yii::app()->name;
  }
}

?>