<?php

/**
 * This is the model class for table "user_link".
 *
 * The followings are the available columns in table 'user_link':
 * @property integer $id
 * @property string $first_user_id
 * @property string $second_user_id
 * @property integer $link_type
 * @property integer $status
 * @property string $create_date
 */
class UserLink extends CActiveRecord
{
	const STATUS_REQUESTED = 0;
	const STATUS_CONFIRMED = 1;
	const STATUS_REJECTED = 2;
	
	const TYPE_FRIEND = 1;
	
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_link';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('first_user_id, second_user_id, link_type, status', 'required'),
			array('link_type, status', 'numerical', 'integerOnly'=>true),
			array('first_user_id, second_user_id', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, first_user_id, second_user_id, link_type, status, create_date', 'safe', 'on'=>'search'),
		);
	}
	
	protected function beforeSave()
	{
		if(!$this->create_date)
			$this->create_date = date('Y-m-d');
			
		return parent::beforeSave();
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'first_user' => array(self::BELONGS_TO, 'User', 'first_user_id'),
			'second_user' => array(self::BELONGS_TO, 'User', 'second_user_id'),
		);
	}
	

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'first_user_id' => 'First User',
			'second_user_id' => 'Second User',
			'link_type' => 'Link Type',
			'status' => 'Status',
			'create_date' => 'Create Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('first_user_id',$this->first_user_id,true);
		$criteria->compare('second_user_id',$this->second_user_id,true);
		$criteria->compare('link_type',$this->link_type);
		$criteria->compare('status',$this->status);
		$criteria->compare('create_date',$this->create_date,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}