<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $email
 * @property string $first_name
 * @property string $last_name
 * @property string $address
 * @property string $state
 * @property string $zip
 * @property string $password
 * @property string $picture
 */
class User extends CActiveRecord
{
	const TYPE_USER = 1;
	const TYPE_ADMIN = 2;

	public $new_password = NULL;
	public $confirm_password = NULL;	

	/**
	 * Returns the static model of the specified AR class.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function findByUsername($username)
	{
		return $this->find('username=:username',array(':username'=>$username));
	}

	private static $_current;

	public static function current() {
	  if(!self::$_current && !Yii::app()->user->isGuest)
	    self::$_current = User::model()->findByPk(Yii::app()->user->id);
	  return self::$_current;
	}

	public static function admin() {
	  return self::current() ? self::current()->isAdmin() : false;
	}

	public function isAdmin() {
	   return $this->type == self::TYPE_ADMIN;
	}

	public static function types() {
	  return array(
	    self::TYPE_USER => 'User',
	    self::TYPE_ADMIN => 'Admin');
	}
	
	public function getTypeName() {
		$types = self::types();
		return !isset($types[$this->type]) ? 'n/a' : $types[$this->type];
	}
	
	public function getLibraryUrl() {
		return Yii::app()->createUrl('library/index',array('id'=>$this->id));
	}

	private $_notifications = null;

	public function getNotifications() {
	       if($this->_notifications != null)
	          return $this->_notifications;

	       $this->_notifications = array();
	       
	       // Find pending friends requests
		   foreach(UserLink::model()->findAll(
				array(
					'condition'=>'second_user_id=:user and status=:status and link_type=:type',
					'with'=>'first_user',
					'params'=>array(':user'=>Yii::app()->user->id,':status'=>UserLink::STATUS_REQUESTED,
							':type'=>UserLink::TYPE_FRIEND))) as $model) {

			  $n = new Notification;
			  $n->model = $model;
			  $n->message = $model->first_user->username . ' wants to be friends';
			  $n->url = Yii::app()->createUrl('friends/approve');
			  $this->_notifications[] = $n;			
		   }
	       
	       
		   // Find pending asset requests
		   foreach(AssetRequest::model()->findAll(
				array(
					'condition'=>'asset.owner_id=:user and status=:status',
					'with'=>'asset',
					'params'=>array(':user'=>Yii::app()->user->id,':status'=>AssetRequest::STATUS_REQUESTED))) as $model) {

				$n = new Notification;
			  $n->model = $model;
			  $n->message = 'Requested Game : '.$model->asset->title->name;
			  $n->url = Yii::app()->createUrl('asset/approve');
			  $this->_notifications[] = $n;			
		   }
		   
	       // Find waiting to ship objects
	       foreach(AssetHistory::model()->findAll(
	         'user_id=:user and not exists(select id from asset_history h2 where t.asset_id=h2.asset_id '.
		 		 'and h2.timestamp>t.timestamp)'
		 ,array(':user'=>Yii::app()->user->id)) as $model) {

		 	if($model->action == AssetQueue::STATUS_WAITING_FOR_SHIP) {
			  $n = new Notification;
			  $n->model = $model;
			  $n->message = 'Ship '.$model->asset->title->name;
			  $n->url = Yii::app()->createUrl('asset/ship',array('id'=>$model->asset_id));
			  $this->_notifications[] = $n;
			} else if($model->action == AssetQueue::STATUS_IN_TRANSIT) {
			  $n = new Notification;
			  $n->model = $model;
			  $n->message = $model->asset->title->name . ' has shipped.  Click when received.';
			  $n->url = Yii::app()->createUrl('asset/receive',array('id'=>$model->asset_id));
			  $this->_notifications[] = $n;
			}
		}

	       return $this->_notifications;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}
	
	private $_friends = null;	
	
	public function isFriend($user) {
		if(is_object($user)) $user = $user->id;
		if(!$this->_friends)
			$this->_friends = Yii::app()->db->createCommand('select second_user_id from user_link where first_user_id=:user and link_type=:type and status=:status')->
				queryColumn(array(':user'=>$this->id,':type'=>UserLink::TYPE_FRIEND,':status'=>UserLink::STATUS_CONFIRMED));
		return in_array($user,$this->_friends);			
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email, username, first_name, last_name, address, city, state, zip, password', 'required'),
			array('username', 'length', 'max'=>30),
			array('email, first_name, last_name, address, city', 'length', 'max'=>255),
			array('state', 'length', 'max'=>2),
			array('zip', 'length', 'max'=>10),
			array('username','unique','message'=>'Username already in use.'),
			array('email','unique','message'=>'Email address already registered.'),
			array('username', 'match', 'pattern' => '/^[a-zA-Z0-9]+$/', 'message'=>'Username contains invalid characters.'),
			array('confirm_password,new_password', 'required', 'on'=>'register'),
			array('confirm_password', 'compare', 'compareAttribute'=>'password', 'on'=>'register', 'message'=>"Passwords do not match."),
			array('new_password,confirm_password', 'length', 'max'=>30),
			array('picture', 'safe'),
			array('invites, default_visibility, default_trade_policy, type', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, email, first_name, last_name, address, state, zip, password, picture', 'safe', 'on'=>'search'),
		);
	}
			
	public function getName() {
      return $this->first_name . ' ' . $this->last_name;	  
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	public function isTitleInLibrary($title) {
		if(is_object($title)) $title = $title->id;
		return Yii::app()->db->createCommand('select count(*) from asset where owner_id=:owner and title_id=:title')
			->queryScalar(array(':owner'=>$this->id,':title'=>$title)) > 0;
	}

	public function isTitleInWishlist($title) {
		if(is_object($title)) $title = $title->id;
		return Yii::app()->db->createCommand('select count(*) from user_wishlist where user_id=:owner and title_id=:title')
			->queryScalar(array(':owner'=>$this->id,':title'=>$title)) > 0;
	}


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'username' => 'Username',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'address' => 'Address',
			'state' => 'State',
			'zip' => 'Zip',
			'password' => 'Password',
			'picture' => 'Picture',
			'invites' => 'Invites',
		);
	}
	
	public function checkPassword($password) {
	  return $this->password == $password || $this->comparePassword($password);
	}


	private function preparePassword($password,$salt = null) {
		if(is_null($salt)) $salt = Generator::randomCode(2);
		for($i = 0; $i < 100; $i++)
			$password = hash('sha256',$password . $salt);
		if($salt)
			$password .= '*' . $salt;
		return $password;
	}
	
	public function getPasswordSalt($password) {
		$star = strpos($password,'*',1);
		return $star ? substr($password,$star+1) : '';
	}
	
	public function comparePassword($password) {
		return $this->password ==
			$this->preparePassword($password,$this->getPasswordSalt($this->password));
	}
	
	protected function beforeSave() {
		if($this->new_password)
			$this->password = $this->preparePassword($this->new_password);			
		
		return parent::beforeSave();
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('state',$this->state,true);
		$criteria->compare('zip',$this->zip,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('picture',$this->picture,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
	
	public function getFormattedAddress()
	{
	    return $this->first_name . ' ' . $this->last_name . '<br/>' . 
	          $this->address . '<br/>' .
	          $this->state . ' ' . $this->zip;
	}
}