<?php

class TradeCommand extends CConsoleCommand {
  
  public function run($args) {
    
    // Do a round of trades   
    $db = Yii::app()->db;
    
    $queueCriteria = new CDbCriteria;
    $queueCriteria->group = 'id';
    $queueCriteria->order = '(select count(id) from asset where asset.owner_id=t.user_id and available=0)-(select count(id) from trade where trade.user_id=t.user_id) desc,(select count(id) from asset where asset.owner_id=t.user_id) desc';
    $queueCriteria->condition = 'priority=:priority';
       
    for($priority = 1; $priority <= 10; $priority++ ) {
      
      $assetsByTitle = array();
      
      $queueCriteria->params = array(':priority'=>$priority);
      
      foreach(UserQueue::model()->findAll($queueCriteria) as $queue) {
        
        if(!isset($assetsByTitle[$queue->title_id]))
           $assetsByTitle[$queue->title_id] = Asset::model()->findAll(array(
           		'condition'=>'available=1 and title_id=:title_id',
                'params'=>array(':title_id'=>$queue->title_id)));
           
        $assets = &$assetsByTitle[$queue->title_id];
        
        foreach($assets as $arrayKey=>$asset) {
                    
          if($asset->owner_id == $queue->user_id)
             continue;

          $transaction = $db->beginTransaction();
           
          try {
            $trade = new Trade();
            $trade->user_id = $queue->user_id;
            $trade->asset_id = $asset->id;
            $trade->status = Trade::STATUS_WAITING_FOR_MAILING;
            $trade->init_date = getdate();
            
            $last_trade = $asset->current_trade;
          
            if($last_trade) {
              $trade->last_trade_id = $last_trade->id;
              $trade->last_user_id = $last_trade->user_id;
            } else
              $trade->last_user_id = $asset->owner_id;
            
            if(!$trade->save()) {
              foreach($trade->getErrors() as $error)
                 var_dump($error);
                 
              throw new Exception('Could not save trade');
            }
            
            if($last_trade) {            
              $last_trade->next_trade_id = $trade->id;
              $last_trade->status = Trade::STATUS_FINISHED;
              $last_trade->finish_date = getdate();
              $last_trade->save();
            }

            $asset->current_trade_id = $trade->id;
            $asset->available = 0;
            $asset->save();

            $queue->delete();
          
            $transaction->commit();
             
            unset($assets[$arrayKey]);
          } catch( Exception $e ) {
            $transaction->rollback();
            die($e);            
          }
          break;          
        }                     
      }
    }
  }
}

?>
