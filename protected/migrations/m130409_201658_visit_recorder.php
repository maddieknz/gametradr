<?php

class m130409_201658_visit_recorder extends CDbMigration
{
	public function up()
	{
		$this->execute(<<<'EOD'
CREATE TABLE IF NOT EXISTS visit (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  visitor_id int(10) unsigned NOT NULL,
  `type` tinyint(4) NOT NULL,
  http_referer varchar(255) CHARACTER SET utf8 NOT NULL,
  ip_address varchar(255) CHARACTER SET utf8 NOT NULL,
  user_agent varchar(255) CHARACTER SET utf8 NOT NULL,
  browser varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  operating_system varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  screen_resolution varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  landing_page varchar(255) CHARACTER SET utf8 NOT NULL,
  visit_start_at datetime NOT NULL,
  last_page varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  last_page_at datetime DEFAULT NULL,
  `language` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  pageviews smallint(6) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY visitor_id (visitor_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS visitor (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  visitor_cookie varchar(64) NOT NULL,
  first_visit_at datetime NOT NULL,
  last_visit_at datetime NOT NULL,
  total_visits smallint(6) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY visitor_cookie (visitor_cookie)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS visit_log (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  visit_id int(10) unsigned NOT NULL,
  user_id int(11) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` varchar(255) NOT NULL,
  url varchar(255) DEFAULT NULL,
  subject_id int(10) unsigned DEFAULT NULL,
  `data` text,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS visit_online (
  visit_id int(10) unsigned NOT NULL,
  visitor_id int(10) unsigned NOT NULL,
  user_id int(11) unsigned DEFAULT NULL,
  visit_start_at datetime NOT NULL,
  last_page_at datetime NOT NULL,
  last_page varchar(255) NOT NULL,
  pageviews mediumint(9) NOT NULL,
  PRIMARY KEY (visit_id),
  KEY user_id (user_id),
  KEY visitor_id (visitor_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `visit`
  ADD CONSTRAINT visit_ibfk_1 FOREIGN KEY (visitor_id) REFERENCES visitor (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `visit_online`
  ADD CONSTRAINT visit_online_ibfk_5 FOREIGN KEY (visitor_id) REFERENCES visitor (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT visit_online_ibfk_2 FOREIGN KEY (user_id) REFERENCES `user` (id) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT visit_online_ibfk_4 FOREIGN KEY (visit_id) REFERENCES visit (id) ON DELETE CASCADE ON UPDATE CASCADE;
EOD
	);
	}

	public function down()
	{
		echo "m130409_201658_visit_recorder does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}