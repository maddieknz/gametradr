<?php

class m130412_220840_convert_cover_picture_to_media extends CDbMigration
{
	public function up()
	{
		// Retrieve cover pictures from titles
		$coverPictures = $this->dbConnection->createCommand('select id,cover_picture,cover_picture_mime_type,submitter_id from title where cover_picture_mime_type!=\'\'')->queryAll();
		
		// Create media objects for the cover pictures
		foreach($coverPictures as $data) {
			$media = new Media;
			$media->title_id = $data['id'];
			$media->file_name = 'cover.' . ($data['cover_picture_mime_type'] == 'image/jpeg' ? 'jpg' : 'png');
			$media->media_type = $data['cover_picture_mime_type'];
			$media->type = Media::TYPE_TITLE_COVER;
			$media->status = Media::STATUS_DEFAULT;
			$media->upload_user_id = $data['submitter_id'];
			$media->data = $data['cover_picture'];
			if(!$media->save()) {
				print_r($media->errors);
				die();
			}
		}
		
		// Remove the columns
		$this->dropColumn('title','cover_picture');
		$this->dropColumn('title','cover_picture_mime_type');		
	}

	public function down()
	{
		echo "m130412_220840_convert_covert_picture_to_media does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}