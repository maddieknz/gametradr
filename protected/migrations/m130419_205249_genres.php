<?php

class m130419_205249_genres extends CDbMigration
{
	public function up()
	{
		$this->execute(<<<'EOD'

CREATE TABLE IF NOT EXISTS `genre` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `title_genre` (
  `title_id` int(11) NOT NULL,
  `genre_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`title_id`,`genre_id`),
  KEY `title_id` (`title_id`),
  KEY `genre_id` (`genre_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `genre`
  ADD CONSTRAINT `genre_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `genre` (`id`);

ALTER TABLE `title_genre`
  ADD CONSTRAINT `title_genre_ibfk_2` FOREIGN KEY (`title_id`) REFERENCES `title` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `title_genre_ibfk_1` FOREIGN KEY (`genre_id`) REFERENCES `genre` (`id`);
EOD
	);

	$this->dropColumn('title','genre');

	}

	public function down()
	{
		echo "m130419_205249_genres does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}