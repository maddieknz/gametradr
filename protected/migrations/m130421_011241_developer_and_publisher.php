<?php

class m130421_011241_developer_and_publisher extends CDbMigration
{
	public function up()
	{
		$this->dropColumn('title','publisher');
		$this->addColumn('title','publisher_id','int unsigned null default null');
		$this->addColumn('title','developer_id','int unsigned null default null');
		$this->addForeignKey('publisher','title','publisher_id','company','id');
		$this->addForeignKey('developer','title','developer_id','company','id');
		$this->addColumn('media','company_id','int unsigned null default null');
		$this->addForeignKey('company','media','company_id','company','id','cascade','cascade');		
	}

	public function down()
	{
		echo "m130421_011241_developer_and_publisher does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}