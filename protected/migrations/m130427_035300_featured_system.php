<?php

class m130427_035300_featured_system extends CDbMigration
{
	public function up()
	{
		$this->addColumn('system','featured_sort_order','tinyint null default null');
	}

	public function down()
	{
		echo "m130427_035300_featured_system does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}