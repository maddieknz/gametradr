<?php

class m130422_163027_url_keys extends CDbMigration
{
	public function up()
	{
		$this->addColumn('title','url_key','varchar(40) null default null');
		$this->addColumn('system','url_key','varchar(40) null default null');
		$this->createIndex('title_url_key','title','url_key',true);
		$this->createIndex('system_url_key','system','url_key',true);
	}

	public function down()
	{
		echo "m130422_163027_url_keys does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}