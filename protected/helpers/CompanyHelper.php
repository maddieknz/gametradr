<?php

class CompanyHelper {
    
     public static function link($company) {
        if(!$company)
            return 'unknown';
        
        $img = $company->name;
        
        // Check for logo
        if(Yii::app()->db->createCommand('select count(*) from media where company_id=:company and type=:type and status=:status')
           ->queryScalar(
	    array(':company'=>$company->id,':type'=>Media::TYPE_COMPANY_LOGO,':status'=>Media::STATUS_DEFAULT)))
            $img = CHtml::image(Yii::app()->createUrl('company/logo',array('id'=>$company->id)),$company->name);
        
        return CHtml::link($img, 
            array('company/view','id'=>$company->id),
            array('class'=>'company-view-link'));        
    }    
    
}