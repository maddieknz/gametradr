<?php 

class Generator {
  

  const GEN_LIST = 'abcdefhjiklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
  
  public static function randomCode($length) {
    $code = '';
    $max = strlen(Generator::GEN_LIST)-1;
    for($i = 0; $i < $length; $i++)
      $code .= substr(Generator::GEN_LIST, rand(0,$max), 1);
    return $code;
  }
  
}

?>
