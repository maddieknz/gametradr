<?php

class UserHelper {
    
    /**
     * Find which titles in an array are also in the users library.  Defaults to the current session user.
     */    
    static function findTitlesInLibrary(array $titles,$user = null) {
        if(empty($titles)) return array();
        if(!$user && !Yii::app()->user->id) return array();
        $titleIds = array();
        foreach($titles as $title)
            $titleIds[] = is_object($title) ? $title->id : (int) $title;
        $titleIds = implode(',',$titleIds);
        $ownedTitles = Yii::app()->db->createCommand('select title_id from asset where owner_id=:owner and title_id in ('.$titleIds.')')->
            queryColumn(array(':owner'=>$user ? $user->id : Yii::app()->user->id));
        $result = array();
        foreach($titles as $title)
            if(in_array($title->id,$ownedTitles)) $result[] = $title;
        return $result;
    }

    /**
     * Find which titles in an array are also in the users library.  Default to the current session suser;
     */    
    static function findTitlesInWishlist(array $titles,$user = null) {
        if(empty($titles)) return array();
        if(!$user && !Yii::app()->user->id) return array();        
        $titleIds = array();
        foreach($titles as $title)
            $titleIds[] = is_object($title) ? $title->id : (int) $title;
        $titleIds = implode(',',$titleIds);
        $wishlistTitles = Yii::app()->db->createCommand('select title_id from user_wishlist where user_id=:owner and title_id in ('.$titleIds.')')->
            queryColumn(array(':owner'=>$user ? $user->id : Yii::app()->user->id));
        $result = array();
        foreach($titles as $title)
            if(in_array($title->id,$wishlistTitles)) $result[] = $title;
        return $result;
    }

    static function userLink($user) {
        if(!$user)
            return 'a user';
        else if($user->id == Yii::app()->user->id)
            return 'You';
        return CHTML::link($user->username,
            Yii::app()->createUrl('user/view',array('id'=>$user->id)),
            array('class'=>'user-profile-link'));        
    }
    
    /*
     * Process a friend request.  If there is an open request from $user2, the friendship is finalized.
     * Otherwise we check for existing friendship or an existing request.
     * @return two element array, first containing success or error, second containing a message.
     */
    public static function processFriendRequest($user1,$user2) {

        if($user1->id == $user2->id)
            return array('error','You cannot be friends with yourself.');
                         				
	// Find existing links
	$status = Yii::app()->db->createCommand('select distinct status from user_link where first_user_id=:first '.
            ' and second_user_id=:second and link_type=:type')->queryColumn(array(
            ':first'=>$user1->id,':second'=>$user2->id,':type'=>UserLink::TYPE_FRIEND));
				
	// Check for pending or confirmed friendship requests
	if(in_array(UserLink::STATUS_CONFIRMED,$status))
            return array('error',"You are already friends with {$user2->username}.");
	
        if(in_array(UserLink::STATUS_REQUESTED,$status))
            return array('error',"You have already requested friendship with {$user2->username}.");

        // Create a new friendship request
        $link = new UserLink;
        $link->first_user_id = $user1->id;
        $link->second_user_id = $user2->id;
        $link->link_type = UserLink::TYPE_FRIEND;
            
       // Is there an open request from the other party?
	$link2 = UserLink::model()->find(
            'first_user_id=:first and second_user_id=:second and link_type=:type and status=:status',
            array(
                ':first'=>$user2->id,
		':second'=>$user1->id,
		':type'=>UserLink::TYPE_FRIEND,
		':status'=>UserLink::STATUS_REQUESTED
		));
								
	if($link2) {
            $link2->status = UserLink::STATUS_CONFIRMED;
            $link2->save();

            $link->status = UserLink::STATUS_CONFIRMED;
            $link->save();            
            
            self::generateNewFriendsStreamMessage($user1,$user2);
            
            return array('success',"You are now friends with {$user2->username}.");
	} else {
            $link->status = UserLink::STATUS_REQUESTED;
            $link->save();            
            return array('success',"A friendship request has been sent to {$user2->username}.");
        }
    }
    
    /*
     * Process the friend request response. $result is one of UserLink::STATUS_CONFIRMED or UserLink::STATUS_REJECTED.
     * @return two element array, first containing success or error, second containing a message.
     */    
    public static function processFriendRequestResponse($user1,$user2,$result)
    {        
        if($user1->id == $user2->id)
            return array('error','You cannot be friends with yourself.');

        $link1 = null;
        $link2 = null;
        
        // Find existing request links
        foreach(UserLink::model()->findAll(
            'first_user_id in (:first,:second) and second_user_id in (:first,:second) and link_type=:type and status in (:status1,:status2)',
            array(
                ':first'=>$user2->id,
		':second'=>$user1->id,
		':type'=>UserLink::TYPE_FRIEND,
		':status1'=>UserLink::STATUS_REQUESTED,
		':status2'=>UserLink::STATUS_CONFIRMED                
		)) as $link) {
                        
            if($link->status == UserLink::STATUS_CONFIRMED)
                return array('error','You are already friends.');
            
            if($link->first_user_id == $user1->id)
                $link2 = $link;
            if($link->first_user_id == $user2->id)
                $link1 = $link;            
        }
        
        if(!$link1)
            return array('error',"You have no open friendship request from {$user2->username}.");
        
        if($result == 'reject') {
            $link1->status = UserLink::STATUS_REJECTED;
            $link1->save();
            return array('success',"You have rejected {$user2->username}'s friend request.");            
        } else if($result != 'approve')
            return array('error',"Invalid friendship response");
                    
        $link1->status = UserLink::STATUS_CONFIRMED;
        $link1->save();
        
        if(!$link2) {
            $link2 = new UserLink;
            $link2->first_user_id = $user1->id;
            $link2->second_user_id = $user2->id;
            $link2->link_type = UserLink::TYPE_FRIEND;
        }

        $link2->status = UserLink::STATUS_CONFIRMED;
        $link2->save();

        self::generateNewFriendsStreamMessage($user1,$user2);
        
        return array('success',"You are now friends with {$user2->username}.");
    }
    
    public static function generateNewFriendsStreamMessage($user1,$user2)
    {
        $stream = new Stream;
        $stream->type = 'NewFriends';
        $stream->visibility =  Stream::VISIBILITY_GLOBAL;
        $stream->typeModel->user1 = $user1->id;
        $stream->typeModel->user2 = $user2->id;      
        $stream->save();
    }
    
}

?>
