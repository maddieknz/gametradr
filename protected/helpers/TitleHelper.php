<?php

class TitleHelper {
    
     public static function titleLink($title) {
        if(!$title)
            return 'a deleted title';
        return CHtml::link($title->name,
            Yii::app()->createUrl('title/view',array('id'=>$title->id)),
            array('class'=>'title-view-link'));        
    }    
    
}