<?php

class EventRegistry extends CApplicationComponent {

      public function onUserLogin(CEvent $event) {
        $this->raiseEvent('onUserLogin',$event);
      }

      public function onUserLogout(CEvent $event) {
        $this->raiseEvent('onUserLogout',$event);
      }
}

?>