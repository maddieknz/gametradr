<?php

Yii::import('zii.widgets.grid.CGridColumn');

/**
 * CLinkColumn represents a grid view column that renders a hyperlink in each of its data cells.
 *
 * The {@link label} and {@link url} properties determine how each hyperlink will be rendered.
 * The {@link labelExpression}, {@link urlExpression} properties may be used instead if they are available.
 * In addition, if {@link imageUrl} is set, an image link will be rendered.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @version $Id$
 * @package zii.widgets.grid
 * @since 1.1
 */
class CImageColumn extends CGridColumn
{
        /**
         * @var string the label to the hyperlinks in the data cells. Note that the label will not
         * be HTML-encoded when rendering. This property is ignored if {@link labelExpression} is set.
         * @see labelExpression
         */
        public $alt='Image';
                
        /**
         * @var string a PHP expression that will be evaluated for every data cell and whose result will be rendered
         * as the URL of the hyperlink of the data cells. In this expression, the variable
         * <code>$row</code> the row number (zero-based); <code>$data</code> the data model for the row;
         * and <code>$this</code> the column object.
         */
        public $urlExpression;
        /**
         * @var array the HTML options for the data cell tags.
         */
        public $htmlOptions=array('class'=>'image-column');
        /**
         * @var array the HTML options for the header cell tag.
         */
        public $headerHtmlOptions=array('class'=>'image-column');
        /**
         * @var array the HTML options for the footer cell tag.
         */
        public $footerHtmlOptions=array('class'=>'image-column');
        /**
         * @var array the HTML options for the hyperlinks
         */
        public $imageHtmlOptions=array();

        /**
         * Renders the data cell content.
         * This method renders a hyperlink in the data cell.
         * @param integer $row the row number (zero-based)
         * @param mixed $data the data associated with the row
         */
        protected function renderDataCellContent($row,$data)
        {
                if($this->urlExpression!==null)
                        $url=$this->evaluateExpression($this->urlExpression,array('data'=>$data,'row'=>$row));
                else
                        $url=$this->url;
                $options=$this->imageHtmlOptions;

                echo CHtml::image($url,$this->alt,$options);
        }
}
