<?php

class GTActiveDataProvider extends CActiveDataProvider {
  
    protected function fetchData()
	{
        $this->beforeFetch();
        $data = parent::fetchData();
        $this->afterFetch($data);
        return $data;
    }
    
    protected function beforeFetch()
    {
        return;
    }
    
    protected function afterFetch($data)
    {
        return;
    }
}

?>