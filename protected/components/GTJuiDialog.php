<?php

Yii::import('zii.widgets.jui.CJuiWidget');

class GTJuiDialog extends CJuiWidget
{
        /**
         * @var string the name of the container element that contains all panels. Defaults to 'div'.
         */
        public $tagName='div';

        public function beginWidget($className, $properties) {
                $id=$this->getId();
                if (isset($this->htmlOptions['id']))
                        $id = $this->htmlOptions['id'];
                else
                        $this->htmlOptions['id']=$id;

                $options=empty($this->options) ? '' : CJavaScript::encode($this->options);
                Yii::app()->getClientScript()->registerScript(__CLASS__.'#'.$id,"jQuery('#{$id}').dialog($options);");
                echo CHtml::openTag($this->tagName,$this->htmlOptions)."\n";
        }

        /**
         * Renders the close tag of the dialog.
         */
        public function run()
        {
                echo CHtml::closeTag($this->tagName);
        }
}
