<?php

class VisitRecorder extends CApplicationComponent {
    
    private $_visit = null;
    private $_visitOnline = null;
    private $_visitor = null;
    public $first_visit = false; // true if this is the first time on site
    public $is_page_view = true;
    public $has_exception = false;
    
    const VISITOR_COOKIE_NAME = 'gt_uv';
    
    public function init() {
        parent::init();

	$this->flushVisitorsOnline();

        // Register the event handler
        Yii::app()->attachEventHandler('onBeginRequest',array($this,'onBeginRequest'));
        Yii::app()->attachEventHandler('onEndRequest',array($this,'onEndRequest'));
        Yii::app()->attachEventHandler('onException',array($this,'onException'));        
    }

    protected function flushVisitorsOnline() {
    	
	if(Yii::app()->cache->get('flush_visitors_online') === false) {

	   // Load the expired visits
	   foreach(VisitOnline::model()->findAll('last_page_at<=:timestamp',array(':timestamp'=>date('Y-m-d h:i:s',
	        time()-30*60))) as $visitOnline) {

		// Copy the closing fields to visit and delete
		$visit = Visit::model()->findByPk($visitOnline->visit_id);
		$visit->last_page = $visitOnline->last_page;
		$visit->last_page_at = $visitOnline->last_page_at;
		$visit->pageviews = $visitOnline->pageviews;
		$visit->save(false,array('last_page','last_page_at','pageviews'));
		$visitOnline->delete();
	   }

	   Yii::app()->cache->add('flush_visitors_online',1,5*60);
	}
    }

    public function getVisit() {
    	if(!$this->_visit && $this->_visitOnline)
	  $this->_visit = Visit::model()->findByPk($this->_visitOnline->visit_id);
    	return $this->_visit;
    }

    public function getVisitor() {
    	if(!$this->_visitor && $this->_visitOnline)
	  $this->_visitor = $this->_visitOnline->visitor;
        return $this->_visitor;
    }


    public function onBeginRequest() {
        
        if(Yii::app()->request->isAjaxRequest)
            $this->is_page_view = false;
            
        $this->setupVisit();      
    }
    
    public function onException($event) {
        $this->has_exception = true;
    }
    
    public function onEndRequest() {
        
        if(!$this->is_page_view)
	   return;

	if(!$this->has_exception) {
            $this->_visitOnline->pageviews++;
	    $this->_visitOnline->user_id = Yii::app()->user->id;
            $this->_visitOnline->last_page_at = date('Y-m-d h:i:s');
            $this->_visitOnline->last_page = Yii::app()->request->url;
            $this->_visitOnline->save(false);
        }

	$log = new VisitLog;
	$log->visit_id = $this->_visitOnline->visit->id;
	$log->user_id = Yii::app()->user->id;
	$error = Yii::app()->errorHandler->error;
	$log->type = 'pageview-' . ($error ? $error['code'] : 200);
	$log->url = Yii::app()->request->url;
	$log->save(false);
    }
    
    private function setupVisit() {
        
        // Do we have a visit id in the session?
        if(Yii::app()->user->hasState('visit_id') &&
	   ($this->_visitOnline = VisitOnline::model()->findByPk(Yii::app()->user->getState('visit_id'))))
	   return;
        
        // If we need to create a visit, first find or create visitor id 
        $this->setupVisitor();
        
        $visit = $this->_visit = new Visit;
        $visit->visitor_id = $this->visitor->id;
        $visit->type = $this->first_visit ? Visit::TYPE_NEW : Visit::TYPE_RETURNING;
        $visit->http_referer = Yii::app()->request->urlReferrer;
        $visit->ip_address = Yii::app()->request->userHostAddress;
        $visit->user_agent = Yii::app()->request->userAgent;
        $visit->language = Yii::app()->request->preferredLanguage;
        $visit->landing_page = $this->visit->last_page = Yii::app()->request->url;
        $visit->visit_start_at = $this->visit->last_page_at = date('Y-m-d h:i:s');
        if(!$this->first_visit) {
            $this->visitor->last_visit_at = $this->visit->visit_start_at;
            $this->visitor->total_visits++;
            $this->visitor->save();
        }        
        $visit->save(false);
	
	$visitOnline = $this->_visitOnline = new VisitOnline;
	$visitOnline->visit_id = $visit->id;
	$visitOnline->visitor_id = $this->visitor->id;
	$visitOnline->visit_start_at = $visitOnline->last_page_at = $visit->visit_start_at;
	$visitOnline->last_page = Yii::app()->request->url;
	$visitOnline->pageviews = 0;
	$visitOnline->save(false);
        
        // Save the visit id in session
        Yii::app()->user->setState('visit_id',$this->visit->id);
    }
    
    private function setupVisitor() {
        
        $cookies = &Yii::app()->request->cookies;
                
        // Do we have a visitor cookie?
        if(isset($cookies[self::VISITOR_COOKIE_NAME]))
            $this->_visitor = Visitor::model()->findByCookie($cookies[self::VISITOR_COOKIE_NAME]->value);
        
        // Setup a new visitor object
        if(!$this->_visitor) {
            $this->first_visit = true;
            $this->_visitor = new Visitor;
            $this->_visitor->visitor_cookie = Generator::randomCode(64);
            $this->_visitor->first_visit_at = $this->_visitor->last_visit_at = date('Y-m-d h:i:s');
            $this->_visitor->total_visits = Yii::app()->request->isAjaxRequest ? 0 : 1;
            $this->_visitor->save();
            
            // store the visitor cookie
            $cookies[self::VISITOR_COOKIE_NAME] = new CHttpCookie(self::VISITOR_COOKIE_NAME,$this->visitor->visitor_cookie,array('expire'=>time()+60*60*24*60));
        }        
    }
}

?>
