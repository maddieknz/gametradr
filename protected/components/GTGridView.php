<?php

Yii::import('zii.widgets.grid.CGridView');

class GTGridView extends CGridView {
  
  public $rowClickUrlExpression = null;
  
  public function init()
  {
		parent::init();
   
        if($this->rowClickUrlExpression) {
            $this->selectableRows = 1;
            $this->selectionChanged = 'function(id) {gtGridViewOnRowClick(id);}';
            
            $script = <<<'EOD'
                function gtGridViewOnRowClick(id) {
                    var grid = $('#'+id);
                    var settings = $.fn.yiiGridView.settings[id];
    				var urls = grid.find('.row-click-urls span'),
    				selection = [];
        			grid.find('.' + settings.tableClass).children('tbody').children().each(function (i) {
        				if ($(this).hasClass('selected')) {
                        location.href = urls.eq(i).text();
                        return;
    				}
    			});
                }                                                     
EOD;
            
            Yii::app()->clientScript->registerScript('gtGridViewOnRowClick',$script,CClientScript::POS_END);                                            
        }   
  }
  
  	/**
	 * Renders the row click URLs in a hidden tag.
	 */
	public function renderRowClickUrls()
	{
		echo CHtml::openTag('div',array(
			'class'=>'row-click-urls',
			'style'=>'display:none',
			'title'=>Yii::app()->getRequest()->getUrl(),
		));
        $row = 0;
        
		foreach($this->dataProvider->getData() as $data) {
			$url=$this->evaluateExpression($this->rowClickUrlExpression,array('data'=>$data,'row'=>$row++));
			echo "<span>".CHtml::encode($url)."</span>";
		}
		echo "</div>\n";
	}
    
    public function run()
	{
		$this->registerClientScript();

		echo CHtml::openTag($this->tagName,$this->htmlOptions)."\n";

		$this->renderContent();
		$this->renderKeys();
        
        if($this->rowClickUrlExpression)
            $this->renderRowClickUrls();

		echo CHtml::closeTag($this->tagName);
	}
}

?>