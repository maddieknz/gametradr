<?php

class ActiveRecord extends CActiveRecord {

  protected function beforeSave() {
    $arrayForeignKeys=$this->tableSchema->foreignKeys;
    foreach ($this->attributes as $name=>$value) {
      if (array_key_exists($name, $arrayForeignKeys) &&
      	 $this->metadata->columns[$name]->allowNull && trim($value)=='') {       
        $this->$name=null;
      }
    }
    return parent::beforeSave();
  }
}

?>