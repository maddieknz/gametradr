<?php

// Global application settings for console
return array_replace_recursive(array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'GameTradr.co',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
 		'application.helpers.*',
		'ext.yii-mail.YiiMailMessage'
	),
	
	'commandMap'=>array(
        'migrate'=>array(
            'class'=>'system.cli.commands.MigrateCommand',
            'migrationPath'=>'application.migrations',
            'migrationTable'=>'migration',
            'connectionID'=>'db',
        )),

	// application components
	'components'=>array(
	   'mail' => array(
             'class' => 'ext.yii-mail.YiiMail',
             'transportType' => 'php',
            ),

	'cache'=>array(
	   'class' => 'system.caching.CDummyCache'
	),
            
	'eventRegistry' => array(
	    'class' => 'application.components.EventRegistry'
	),
        
        'log'=>array(
            'class'=>'CLogRouter',
        )
	),

	'params'=>array(
		'adminEmail'=>'no-reply@gametradr.co',
	),
),require('local.php'));