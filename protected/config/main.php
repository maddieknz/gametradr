<?php

// Global application settings
return array_replace_recursive(array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'GameTradr.co',

	// preloading 'log' component
	'preload'=>array('log','visitRecorder'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.models.forms.*',
		'application.components.*',
        'application.helpers.*',
        'ext.yii-mail.YiiMailMessage'
  ),
	
	// application components
	'components'=>array(
		'mail' => array(
             'class' => 'ext.yii-mail.YiiMail',
             'transportType' => 'php',
            ),

	'cache'=>array(
	   'class' => 'system.caching.CDummyCache'
	),
            
    	'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
        
        'visitRecorder' => array(
            'class' => 'application.components.VisitRecorder'  
        ),

	'eventRegistry' => array(
	    'class' => 'application.components.EventRegistry'
	),

		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'rules'=>array(
                            'users/<username:\w+>'=>'user/view',
                            'users/<username:[a-zA-Z0-9]+>/library'=>'library/index',
                            'users/<username:[a-zA-Z0-9]+>/wishlist'=>'wishlist/index',
                            'titles/<url_key:[a-z0-9-]+>'=>'title/view',
                            'systems/<url_key:[a-z0-9-]+>'=>'title/system',
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
            'errorAction'=>'site/error',
        ),
        
        'log'=>array(
            'class'=>'CLogRouter',
        )
	),

	'params'=>array(
		'adminEmail'=>'no-reply@gametradr.co',
	),
),require('local.php'));