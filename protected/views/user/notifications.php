<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Notifications',
);

?>

<h1>Notifications</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'notifications-grid',
	'dataProvider'=>new CArrayDataProvider($user->notifications),
	'columns'=>array(
		'message',
		array(
			'header'=>'Action',
			'class'=>'CLinkColumn',
			'label'=>'Go',
			'urlExpression'=>'$data->url')
	),
)); ?>
