<?php
$this->breadcrumbs=array(
	'Users',
);

$this->menu=array(
	array('label'=>'Create User', 'url'=>array('create')),
	array('label'=>'Manage User', 'url'=>array('admin')),
);
?>

<h1>Users</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'=>$dataProvider,
    'columns'=>array(
      'name',


	array(
    	'class'=>'CLinkColumn',
   		'header'=>'Actions',
    	'labelExpression'=>'"Add to Friends List"',
    	'urlExpression'=>'Yii::app()->createUrl("friends/add",array("user_id"=>$data->id))'
	),

	array(
    	'class'=>'CLinkColumn',
   		'header'=>'Actions',
    	'labelExpression'=>'"View Library"',
    	'urlExpression'=>'Yii::app()->createUrl("library/index",array("user_id"=>$data->id))'
	),

    )
));  ?>
