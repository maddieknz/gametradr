<?php
$this->breadcrumbs=array(
	'Titles'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Title', 'url'=>array('index')),
	array('label'=>'Create Title', 'url'=>array('create')),
);
?>

<h1>Manage Titles</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'title-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		array(
			'class'=>'CImageColumn',
			'header'=>'Cover',
			'headerHtmlOptions'=>array('style'=>'width:100px;'),
			'imageHtmlOptions'=>array('style'=>'max-height: 80px; max-width: 100px;'),			
		    'urlExpression'=>'Yii::app()->createUrl("title/coverPicture",array("id"=>$data->id))'
		),
		array(
			'name'=>'system_id',
			'value'=>'$data->system->name',
			'filter'=>CHtml::activeDropdownList($model,'system_id',System::listData(),array('prompt'=>'[ALL]'))
		),
		'name',
		'publisher',
/*		array(
			'name'=>'developer_id',
			'value'=>'$data->developer->name',
			'filter'=>CHtml::activeDropdownList($model,'developer_id',Company::listData(),array('prompt'=>'[ALL]'))
		),*/
		array(
			'name'=>'status',
			'value'=>'$data->getStatusLabel()',
			'filter'=>CHtml::activeDropdownList($model,'status',Title::statusLabels(),array('prompt'=>'[ALL]'))
		),
		array(
			'name'=>'submitter.username',
			'header'=>'Submitter'
		),
		'submit_date',
		
		array(
			'class'=>'CButtonColumn',
			'buttons'=>array('media'=>array(
				'label'=>'Media',
				'url'=>'Yii::app()->createUrl("admin/media/index",array("Media[title_id]"=>$data->id))'
			)),
			'template'=>'{view} {update} {delete} {media}'
		),
		
	),
)); ?>
