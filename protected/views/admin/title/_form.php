<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(	
	'id'=>'title-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data')
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
	
	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name'); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'url_key'); ?>
		<?php echo $form->textField($model,'url_key'); ?>
		<?php echo $form->error($model,'url_key'); ?>
		<p class="note">Automatically created based on name if not entered.</p>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'system_id'); ?>
		<?php echo $form->listBox($model,'system_id',System::listData()); ?>
		<?php echo $form->error($model,'system_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'publisher_id'); ?>
		<?php echo $form->dropDownList($model,'publisher_id',Company::listData(),array('prompt'=>'--SELECT--')); ?>
		<?php echo $form->error($model,'publisher_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'developer_id'); ?>
		<?php echo $form->dropDownList($model,'developer_id',Company::listData(),array('prompt'=>'--SELECT--')); ?>
		<?php echo $form->error($model,'developer_id'); ?>
	</div>	

	<div class="row">
		<?php echo $form->labelEx($model,'genres'); ?>
		<?php echo $form->listBox($model,'genres',Genre::listData(),
				array('multiple'=>'multiple','size'=>8)); ?>
		<?php echo $form->error($model,'genres'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description'); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'release_date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
		      'model'=>$model,
		      'attribute'=>'release_date',
    			// additional javascript options for the date picker plugin
    			'options'=>array(
        		'showAnim'=>'slide',
				'dateFormat'=>'yy-mm-dd',
				'changeMonth'=>'true',
				'changeYear'=>'true',
				
    			),
    			'htmlOptions'=>array(
        		'style'=>'height:20px;'
    			),
		));?>
		<?php echo $form->error($model,'release_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'number_of_players'); ?>
		<?php echo $form->textField($model,'number_of_players',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'number_of_players'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cover_picture'); ?>		
		<?php echo $form->fileField($model,'cover_picture'); ?>
		<?php echo $form->error($model,'cover_picture'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',Title::statusLabels()); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

<?php /*
	<div class="row">
		<?php echo $form->labelEx($model,'submit_date'); ?>
		<?php echo $form->textField($model,'submit_date'); ?>
		<?php echo $form->error($model,'submit_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'approve_date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
    			'name'=>'approve_Date',
    			// additional javascript options for the date picker plugin
    			'options'=>array(
        		'showAnim'=>'slide',
				'dateFormat'=>'yy-mm-dd',
				'changeMonth'=>'true',
				'changeYear'=>'true',
				
    			),
    			'htmlOptions'=>array(
        		'style'=>'height:20px;'
    			),
		));?>
		<?php echo $form->error($model,'approve_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'reject_date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
    			'name'=>'reject_Date',
				
    			// additional javascript options for the date picker plugin
    			'options'=>array(
        		
				'dateFormat'=>'yy-mm-dd',
				'changeMonth'=>'true',
				'changeYear'=>'true',
				
    			),
    			'htmlOptions'=>array(
        		'style'=>'height:20px;'
    			),
		));?>
		<?php echo $form->error($model,'reject_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'reject_comments'); ?>
		<?php echo $form->textArea($model,'reject_comments',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'reject_comments'); ?>
	</div> */ ?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->