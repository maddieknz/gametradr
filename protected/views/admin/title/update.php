<?php
$this->breadcrumbs=array(
	'Titles'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Title', 'url'=>array('index')),
	array('label'=>'Create Title', 'url'=>array('create')),
	array('label'=>'View Title', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Title', 'url'=>array('admin')),
);
?>

<h1>Update Information for <?php echo $model->name; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>