<?php
/* @var $this CompanyController */
/* @var $model Company */

$this->breadcrumbs=array(
	'Companies'=>array('index'),
	'Manage',	
);

$this->menu=array(
	array('label'=>'List Company', 'url'=>array('index')),
	array('label'=>'Create Company', 'url'=>array('create')),
);

?>

<h1>Manage Companies</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'company-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		array(
			'class'=>'CImageColumn',
			'header'=>'Logo',
			'headerHtmlOptions'=>array('style'=>'width:60px;'),
			'imageHtmlOptions'=>array('style'=>'max-height: 50px; max-width: 60px;'),			
			'urlExpression'=>'Yii::app()->createUrl("company/logo",array("id"=>$data->id))'
		),		
		'name',
		'website_url',
		array(
			'class'=>'CButtonColumn',
			'buttons'=>array('media'=>array(
				'label'=>'Media',
				'url'=>'Yii::app()->createUrl("admin/media/index",array("Media[company_id]"=>$data->id))'
			)),
			'template'=>'{view} {update} {delete} {media}'
		),
	),
)); ?>
