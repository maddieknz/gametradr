<?php
/* @var $this GenreController */
/* @var $model Genre */

$this->breadcrumbs=array(
	'Genres'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Genre', 'url'=>array('index')),
	array('label'=>'Create Genre', 'url'=>array('create')),
);
?>

<h1>Manage Genres</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'genre-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		array(
		  'name'=>'parent_id',
		  'value'=>'$data->parent ? $data->parent->name : ""'
		),
		'name',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
