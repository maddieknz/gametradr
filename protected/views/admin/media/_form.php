<?php
/* @var $this MediaController */
/* @var $model Media */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'media-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data')	
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'title_id'); ?>
		<?php echo $form->textField($model,'title_id'); ?>
		<?php echo $form->error($model,'title_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_id'); ?>
		<?php echo $form->textField($model,'user_id',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'system_id'); ?>
		<?php echo $form->textField($model,'system_id',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'system_id'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'upload_date'); ?>
		<?php echo $form->textField($model,'upload_date'); ?>
		<?php echo $form->error($model,'upload_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php echo $form->dropDownList($model,'type',Media::typeLabels(),array('prompt'=>'-- Select --')); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',Media::statusLabels(),array('prompt'=>'-- Select --')); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'data'); ?>
		<?php echo $form->fileField($model,'data'); ?>
		<?php echo $form->error($model,'data'); ?>
	</div>

	<div style="margin:10px 10px 10px -30px;width:400px;border:1px solid black;padding:5px">
		
	<p class="note">These properties will be set when uploading a new file.</p>
	<div>
		<?php echo $form->labelEx($model,'file_name'); ?>
		<?php echo $form->textField($model,'file_name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'file_name'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'media_type'); ?>
		<?php echo $form->textField($model,'media_type',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'media_type'); ?>
	</div>

	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sort_order'); ?>
		<?php echo $form->textField($model,'sort_order'); ?>
		<?php echo $form->error($model,'sort_order'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'media_url'); ?>
		<?php echo $form->textField($model,'media_url',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'media_url'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->