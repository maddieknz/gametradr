<?php
/* @var $this MediaController */
/* @var $model Media */

$this->breadcrumbs=array(
	'Medias'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Media', 'url'=>array('index')),
	array('label'=>'Create Media', 'url'=>array_merge(array('create'),$this->getSearchAttributeValues($model))),
);
?>

<h1>Manage Media</h1>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'media-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data')	
)); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'title_id'); ?>
		<?php echo $form->textField($model,'title_id'); ?>
		<?php echo $form->error($model,'title_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_id'); ?>
		<?php echo $form->textField($model,'user_id',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'system_id'); ?>
		<?php echo $form->textField($model,'system_id',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'system_id'); ?>
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->	

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'media-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
/*		'title_id',
		'user_id',
		'system_id',*/
		array(
			'class'=>'CImageColumn',
			'header'=>'Preview',
			'headerHtmlOptions'=>array('style'=>'width:100px;'),
			'imageHtmlOptions'=>array('style'=>'max-height: 80px; max-width: 100px;'),			
			'urlExpression'=>'Yii::app()->createUrl("admin/media/view",array("id"=>$data->id))'
		),
		'file_name',
		'media_type',		
		'upload_date',
		'type',
		'status',
		array(
			'name'=>'upload_user_id',
			'value'=>'$data->uploadUser->username'
		),
		'description',
		'sort_order',
		'media_url',
		array(
			'class'=>'CButtonColumn'
		)
	),
)); ?>
