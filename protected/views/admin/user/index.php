<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create User', 'url'=>array('create')),
);
?>

<h1>Manage Users</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'email',
		array(
			'name'=>'type',
			'value'=>'$data->getTypeName()',
			'filter'=>CHtml::activeDropdownList($model,'type',User::types(),array('prompt'=>'[ALL]'))
		),
		'username',
		'first_name',
		'last_name',
		'state',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
