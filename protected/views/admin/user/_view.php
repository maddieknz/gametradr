<?php
/* @var $this UserController */
/* @var $data User */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type')); ?>:</b>
	<?php echo CHtml::encode($data->type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('username')); ?>:</b>
	<?php echo CHtml::encode($data->username); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_name')); ?>:</b>
	<?php echo CHtml::encode($data->first_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_name')); ?>:</b>
	<?php echo CHtml::encode($data->last_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address')); ?>:</b>
	<?php echo CHtml::encode($data->address); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('city')); ?>:</b>
	<?php echo CHtml::encode($data->city); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('state')); ?>:</b>
	<?php echo CHtml::encode($data->state); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('zip')); ?>:</b>
	<?php echo CHtml::encode($data->zip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('password')); ?>:</b>
	<?php echo CHtml::encode($data->password); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('picture')); ?>:</b>
	<?php echo CHtml::encode($data->picture); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('inventory')); ?>:</b>
	<?php echo CHtml::encode($data->inventory); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('games_out')); ?>:</b>
	<?php echo CHtml::encode($data->games_out); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('games_in')); ?>:</b>
	<?php echo CHtml::encode($data->games_in); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('invites')); ?>:</b>
	<?php echo CHtml::encode($data->invites); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('default_trade_policy')); ?>:</b>
	<?php echo CHtml::encode($data->default_trade_policy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('default_visibility')); ?>:</b>
	<?php echo CHtml::encode($data->default_visibility); ?>
	<br />

	*/ ?>

</div>