<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'system-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data')
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'url_key'); ?>
		<?php echo $form->textField($model,'url_key'); ?>
		<?php echo $form->error($model,'url_key'); ?>
		<p class="note">Automatically created based on name if not entered.</p>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'developer_id'); ?>
		<?php echo $form->dropDownList($model,'developer_id',CHtml::listData(
		      Company::model()->findAll(),'id','name'),array('prompt'=>'-- Select --')); ?>
		<?php echo $form->error($model,'developer_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'release_date'); ?>
		<?php echo $form->textField($model,'release_date'); ?>
		<?php echo $form->error($model,'release_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'picture'); ?>
		<?php echo $form->fileField($model,'picture'); ?>
		<?php echo $form->error($model,'picture'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'featured_sort_order'); ?>
		<?php echo $form->textField($model,'featured_sort_order'); ?>
		<?php echo $form->error($model,'featured_sort_order'); ?>
	</div>	

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->