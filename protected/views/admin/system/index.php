<?php
$this->breadcrumbs=array(
	'Systems'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List System', 'url'=>array('index')),
	array('label'=>'Create System', 'url'=>array('create')),
);

?>
<h1>Manage Systems</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'system-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		array(
			'class'=>'CImageColumn',
			'urlExpression'=>'Yii::app()->createUrl(\'/system/image\',array(\'id\'=>$data->id))',
			'imageHtmlOptions'=>array('style'=>'max-width:80px;max-height:80px;')
		),
		'name',
		array(
			'name'=>'developer_id',
			'value'=>'$data->developer->name',
			'filter'=>CHtml::activeDropdownList($model,'developer_id',CHtml::listData(Company::model()->findAll(),'id','name'),array('prompt'=>'[ALL]'))
		),
		'release_date',
		array(
			'class'=>'CButtonColumn',
			'buttons'=>array('media'=>array(
				'label'=>'Media',
				'url'=>'Yii::app()->createUrl("admin/media/index",array("Media[system_id]"=>$data->id))'
			)),
			'template'=>'{view} {update} {delete} {media}'
		),
	),
)); ?>
