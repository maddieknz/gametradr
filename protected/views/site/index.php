<?php if(!Yii::app()->user->isGuest):	 ?>
<h3>Welcome back, <?php echo User::current()->name	 ?></h3>
	<div class="row">
		<div class="span9">
			<div class="row">
				<div class="span9">
				<div class="latest_news">
					<ul class="nav nav-tabs">
  					<li class="active"><a href="#featured" data-toggle="tab">Featured</a></li>
  					<li><a href="#popular" data-toggle="pill">Most Popular</a></li>
  					<li><a href="#newreleases" data-toggle="tab">New Releases</a></li>
  					<li><a href="#comingsoon" data-toggle="tab">Coming Soon</a></li>
					</ul>
					<div id="featured">
					
					</div>
					<div id="popular">
					</div>
					<div id="newreleases">
					</div>
					<div id="comingsoon">
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="span3">
				<div class="reviews">
					<h4>Reviews</h4>

				</div>
			</div>
			<div class="span3">
				<div class="reviews">
					<h4>Reviews</h4>

				</div>
			</div>
			<div class="span3">
				<div class="reviews">
					<h4>Reviews</h4>

				</div>
			</div>
		</div>
	</div>
	<div class="span3">
		<h4>Friends</h4>
	</div>
</div>
<?php else: ?>
	<h3>Welcome!</h3>
	<div class="row">
		<div class="span12">
			<div class="row">
				<div class="span12">
				<div class="latest_news">
					<ul class="nav nav-tabs">
  					<li class="active"><a href="#featured" data-toggle="tab">Featured</a></li>
  					<li><a href="#popular" data-toggle="pill">Most Popular</a></li>
  					<li><a href="#newreleases" data-toggle="tab">New Releases</a></li>
  					<li><a href="#comingsoon" data-toggle="tab">Coming Soon</a></li>
					</ul>
					<div id="featured">
					HI
					</div>
					<div id="popular">
					</div>
					<div id="newreleases">
					</div>
					<div id="comingsoon">
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="span3">
				<div class="reviews">
					<h4>Reviews</h4>

				</div>
			</div>
			<div class="span3">
				<div class="reviews">
					<h4>Reviews</h4>

				</div>
			</div>
			<div class="span3">
				<div class="reviews">
					<h4>Reviews</h4>

				</div>
			</div>
		</div>
	</div>
</div>

<?php endif; ?>

