<?php
$this->breadcrumbs=array(
	'Trade',
);?>

<h1>Games to send:</h1>

<?php
if(empty($gamesToSend))
  echo "None";
else
  foreach($gamesToSend as $trade)
	  $this->renderPartial('_asset',
			array('trade'=>$trade));
?>

<br/><h1>Games to acknowledge receipt:</h1>

<?php
if(empty($gamesToAck))
  echo "None";
else
  foreach($gamesToAck as $trade)
	  $this->renderPartial('_asset',
			array('trade'=>$trade));
?>

<br/><br/><h1>Games borrowed:</h1>

<?php
if(empty($gamesReceived))
  echo "None";
else
  foreach($gamesReceived as $trade)
	  $this->renderPartial('_asset',
			array('trade'=>$trade));
?>

