<h4><?php echo $trade->asset->title->name ?></h4>

<?php if($trade->status == Trade::STATUS_WAITING_FOR_MAILING): ?>
<em>Send to:</em><br/>
<p>
<?php echo $trade->user->getFormattedAddress(); ?>
</p>
<?php elseif($trade->status == Trade::STATUS_IN_TRANSIT): ?>
<em>Mailed to you on :</em> <?php echo $trade->mail_date ?>
<?php elseif($trade->status == Trade::STATUS_RECEIVED || $trade->status == TRADE::STATUS_READY_FOR_NEXT_MAILING): ?>
<em>Date received :</em> <?php echo $trade->receive_date ?><br/>
<em>Ready to return : </em> <?php echo $trade->status == TRADE::STATUS_READY_FOR_NEXT_MAILING ? 'Yes' : 'No' ?>
<?php endif; ?>

<br/>

<?php if($trade->status != TRADE::STATUS_READY_FOR_NEXT_MAILING): ?>
<?php echo CHtml::link('Update Status', array('trade/update','trade_id'=>$trade->id)); ?>
<?php endif; ?>

<br/>