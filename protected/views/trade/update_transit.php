<h1>Update Trade</h1>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'trade-form',
	'enableAjaxValidation'=>false,
)); ?>

<?php echo $form->errorSummary($trade); ?>

<label>Enter the date you received the game below:</label><br/><br/>

<div class="row">
	<?php echo $form->labelEx($trade,'receive_date'); ?>
	<?php echo $form->textField($trade,'receive_date',array('size'=>15,'maxlength'=>15)); ?>
	<?php echo $form->error($trade,'receive_date'); ?>
</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Update'); ?>
	</div>

<?php $this->endWidget(); ?>