<h1>Update Trade</h1>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'trade-form',
	'enableAjaxValidation'=>false,
)); ?>

<?php echo $form->errorSummary($trade); ?>

<label>Do you wish to make this game as ready to return?</label><br/><br/>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Mark Ready',array('name'=>'ready')); ?>
	</div>

<?php $this->endWidget(); ?>