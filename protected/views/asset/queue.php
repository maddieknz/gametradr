<?php
$this->breadcrumbs=array(
	'Queue',
);

?>

<h1>Queue for <?php echo $asset->owner->name; ?>'s copy of <?php echo $asset->title->name; ?></h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'=>new CArrayDataProvider($asset->queue),
    'columns'=>array(
		array(
		'header'=>'Trade #',
		'name'=>'seq_num'),
		array(
		'header'=>'Borrower',
		'name'=>'user.name'),
		array(
		'header'=>'Status',
		'value'=>'$data->getGenericStatus()'),
		'duration',
		'requested_date')));

?>

<a href="<?php echo $this->createUrl('request',array('id'=>$asset->id)); ?>">Request this copy</a>

