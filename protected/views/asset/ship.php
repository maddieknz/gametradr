<h1>Ship Game</h1>

<form method="post">

<div class="row">
     <label>Title:</label>
     <span><?php echo $asset->title->name; ?></span>
</div>

<div class="row">
     <label>Title Owner:</label>
     <span><?php echo $asset->owner->name; ?></span>
</div>

<div class="row">
     <label>Recipient:</label>
     <span><?php echo $asset->lastHistory->queue ? $asset->lastHistory->queue->user->name : $asset->owner->name; ?></span>
</div>

<div class="row">
     <label>Address:</label>
     <span><?php echo $asset->lastHistory->queue ? $asset->lastHistory->queue->user->getFormattedAddress() : $asset->owner->getFormattedAddress(); ?></span>
</div>

<button type="submit" name="submit" value="1">Mark Shipped</button>

</form>