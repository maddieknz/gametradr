<?php
$this->breadcrumbs=array(
	'Queue',
);

?>

<h1>Pending game requests</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'=>$dataProvider,
    'columns'=>array(
		'user.name',
		'asset.title.name',
		'request_date',
		'duration',
array(
    'class'=>'CLinkColumn',
    'header'=>'Actions',
    'labelExpression'=>'"Approve"',
    'urlExpression'=>'Yii::app()->createUrl("asset/approve",array("id"=>$data->id,"status"=>AssetRequest::STATUS_ACCEPTED))'
),
array(
    'class'=>'CLinkColumn',
    'header'=>'Actions',
    'labelExpression'=>'"Reject"',
    'urlExpression'=>'Yii::app()->createUrl("asset/approve",array("id"=>$data->id,"status"=>AssetRequest::STATUS_REJECTED))'
))));
?>


