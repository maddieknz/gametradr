<?php
$this->breadcrumbs=array(
	'Queue',
);

?>

<h1>My game trading</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'=>$dataProvider,
    'columns'=>array(
		array(
			'name'=>'asset.title.name',
			'header'=>'Title'),
		array(
			'name'=>'asset.owner.name',
			'header'=>'Owner'),
		array(
			'class'=>'CLinkColumn',
			'header'=>'Status',
			'labelExpression'=>'$data->getUserStatus()',
			'urlExpression'=>'Yii::app()->createUrl("asset/queue",array("id"=>$data->asset->id))'),
		'duration',
		'requested_date',
		array(
			'header'=>'Actions',
			'class'=>'CLinkColumn',
			'label'=>'Update',
			'urlExpression'=>'$data->getUserActionUrl()',			
		))));
?>



