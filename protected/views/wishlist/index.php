<?php
$this->breadcrumbs=array(
	'Queue',
);

?>

<h1><?php echo $user->id==Yii::app()->user->id ? 'My' : $user->username."'s"; ?> Wish List</h1>

<?php echo CHtml::form(array('queue/index')); ?>

<?php if(Yii::app()->user->hasFlash('queue-add')):?>
    <div class="info">
        <?php echo Yii::app()->user->getFlash('queue-add'); ?>
    </div>
<?php endif; ?>

<?php $columns = array(
      'title.name',
array(
    'class'=>'CImageColumn',
    'header'=>'Cover Picture',
	'imageHtmlOptions'=>array('style'=>'max-width:100px;max-height:100px;'),
    'urlExpression'=>'Yii::app()->createUrl("title/coverPicture",array("id"=>$data->title->id))'
));

if($user->id==Yii::app()->user->id)
	$columns = array_merge($columns,array(
		array(
		'class'=>'CLinkColumn',
		'header'=>'Copies',
		'labelExpression'=>'$data->countCopies()',
		'urlExpression'=>'Yii::app()->createUrl("title/copies",array("id"=>$data->title_id))'
),
array(
    'class'=>'GTTextFieldColumn',
    'name'=>'priority',
    'textFieldHtmlOptions'=>array('size'=>3,'maxlen'=>3),
     'htmlOptions'=>array('style'=>'width:40px')),
   array(
    'class'=>'CLinkColumn',
    'header'=>'Actions',
    'labelExpression'=>'"Remove"',
    'urlExpression'=>'Yii::app()->createUrl("queue/remove",array("title_id"=>$data->title_id))',
))); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'=>$dataProvider,
    'columns'=> $columns));
?>

<?php if($user->id==Yii::app()->user->id): ?>
<?php echo CHtml::submitButton('Update Priority'); ?>
<?php endif; ?>

<?php echo CHtml::endForm(); ?>
