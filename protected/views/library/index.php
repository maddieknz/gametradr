<?php

$this->breadcrumbs=array(
	'Library',
);

Yii::app()->getClientScript()->registerScriptFile("/js/gt.js");

?>


<h1><?php echo ($user->id == Yii::app()->user->id) ? 'My' : $user->username."'s" ?> Library</h1>

<?php if(Yii::app()->user->hasFlash('library-add')):?>
    <div class="info">
        <?php echo Yii::app()->user->getFlash('library-add'); ?>
    </div>
<?php endif; ?>

<?php $columns =
array('title.name',
array(
    'class'=>'CImageColumn',
    'header'=>'Cover Picture',
	'headerHtmlOptions'=>array('style'=>'width:120px;'),
    'urlExpression'=>'Yii::app()->createUrl("title/coverPicture",array("id"=>$data->title->id))'
),
array(
	'class'=>'CLinkColumn',
	'header'=>'Trade Status',
	'labelExpression'=>'$data->lastHistory?$data->lastHistory->getStatusLabel():"Never Traded"',
	'urlExpression'=>'Yii::app()->createUrl("asset/queue",array("id"=>$data->id))'	
),
array(
	'header'=>'Who Has It',
	'value'=>'$data->getCurrentUser()->name'
),
    'title.system.name');

	if($user->id == Yii::app()->user->id)
	$columns = array_merge($columns,array(
		array('header'=>'Trade Policy','value'=>'Asset::tradeOption($data->trade_policy)'),
      array('header'=>'Visiblity','value'=>'Asset::visibilityOption($data->visibility)'),
      array(
    'class'=>'CLinkColumn',
    'header'=>'Actions',
    'labelExpression'=>'"Change Settings"',
    'urlExpression'=>'Yii::app()->createUrl("library/ajaxChange",array("asset_id"=>$data->id))',
    'linkHtmlOptions'=>array('onclick'=>'javascript:ajaxDialog(this);return false;','title'=>'Change Settings')    
),
      array(
    'class'=>'CLinkColumn',
    'header'=>'Actions',
    'labelExpression'=>'"Remove"',
    'urlExpression'=>'Yii::app()->createUrl("library/remove",array("asset_id"=>$data->id))',
)
		
	));
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'=>$dataProvider,
    'columns'=>$columns)); ?>
