<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'change-settings-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($asset); ?>

	<div class="row">
		<?php echo $form->labelEx($asset,'trade_policy'); ?>
		<?php echo $form->dropDownList($asset,'trade_policy',Asset::tradeOptions()); ?>
		<?php echo $form->error($asset,'trade_policy'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($asset,'visibility'); ?>
		<?php echo $form->dropDownList($asset,'visibility',Asset::visibilityOptions()); ?>
		<?php echo $form->error($asset,'visibility'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->