<?php if($html = $data->typeModel->renderMessage()): ?>
<li class="stream-message stream-<?php echo InflectorHelper::decamelize($data->type,'-'); ?>">
    <?php echo $html; ?>
    <div class="stream-message-age"><?php echo $data->getAgeText(); ?></div>
</li>
<?php /*else: ?>
<li class="stream-message stream-message-invalid">No Data</li>
<?php */ endif; ?>
