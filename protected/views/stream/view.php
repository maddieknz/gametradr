<div class="stream-message-full stream-<?php echo InflectorHelper::decamelize($model->type,'-'); ?>">
    <?php echo $model->typeModel->renderFullPage(); ?>
    <div class="stream-message-age"><?php echo $model->getAgeText(); ?></div>
</div>