<li>
    <?php echo CHtml::image(CHtml::normalizeUrl(array('title/coverPicture','id'=>$data->id))); ?>
	<br />
	<br />

	<?php echo CHtml::link(CHtml::encode($data->name),
	      $data->getViewUrl()); ?>

<?php if(!Yii::app()->user->isGuest): ?>
	<span style="position:relative" class="title-add-to"><a href="#" onclick="jQuery(this).next().toggle();return false;">(+)</a>
		<div style="position:absolute;display:none;padding-top:1em;top:0px;">
			<div class="title-add-to-popup" style="border:1px solid black;padding: 3px; background: white;width:140px;">
	<?php if(in_array($data,$ownedTitles)): ?>
		Added to Library
	<?php else: ?>
	<?php echo CHtml::link('+ Add to library', array('library/add', 'title_id'=>$data->id), array('class'=>'title-add-to-library')); ?>
	<?php endif; ?><br/>
	<?php if(in_array($data,$wishlistTitles)): ?>
		Added to Wishlist
	<?php else: ?>
	<?php echo CHtml::link('+ Add to wishlist', array('wishlist/add', 'title_id'=>$data->id), array('class'=>'title-add-to-wishlist')); ?>
	<?php endif; ?>
			</div>
		</div>
	</span>
<?php endif; ?>
</li>

