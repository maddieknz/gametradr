<div class="row">

	<h1><?php echo $data->name; ?></h1>
	<div class="span3">
    <?php echo CHtml::image(CHtml::normalizeUrl(array('title/coverPicture','id'=>$data->id))); ?>

	</div>

	<div class="span6">
	<b><?php echo CHtml::encode($data->getAttributeLabel('publisher_id')); ?>:</b>
	<?php echo CompanyHelper::link($data->publisher); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('developer_id')); ?>:</b>
	<?php echo CompanyHelper::link($data->developer); ?>
	<br />	
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('genres')); ?>:</b>
	<?php if(empty($data->genres)): ?>
	    None
	<?php else: ?>
	<?php foreach($data->genres as $genre): ?>
		<?php echo CHtml::encode($genre->name); ?><br/>
	<?php endforeach; ?>
	<?php endif; ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('release_date')); ?>:</b>
	<?php echo CHtml::encode($data->release_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('number_of_players')); ?>:</b>
	<?php echo CHtml::encode($data->number_of_players); ?>
	<br />
	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<p><?php echo CHtml::encode($data->description); ?></p>
	<br />
</div>

</div>
<?php if(!Yii::app()->user->isGuest): ?>
<div class="row">
	<div class="span6 offset3">
	<?php if(User::current()->isTitleInLibrary($data)): ?>
		<button class="btn btn-primary disabled">You own this title</button>
	<?php else: ?>
	<?php echo CHtml::link('Add to library', array('library/add', 'title_id'=>$data->id),array('class'=>'btn btn-primary')); ?>
	<?php endif; ?>
	<?php if(User::current()->isTitleInWishlist($data)): ?>
		<button class="btn btn-primary disabled">In your wishlist</button>
	<?php else: ?>
	<?php echo CHtml::link('Add to wishlist', array('wishlist/add', 'title_id'=>$data->id),array('class'=>'btn btn-primary')); ?>
	<?php endif; ?>
	</div>
</div>

<hr>

<h3>Copies in your Network</h3>

<?php $copiesData = $data->getCopiesDataProvider(); ?>

<?php $this->renderPartial('_copies',array('dataProvider'=>$copiesData)); ?>
<?php endif; ?>
