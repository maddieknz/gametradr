<?php
$this->breadcrumbs=array(
	'Titles'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Title', 'url'=>array('index')),
	array('label'=>'Create Title', 'url'=>array('create')),
	array('label'=>'Update Title', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Title', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Title', 'url'=>array('admin')),
);
?>



<?php echo $this->renderPartial('_single', array('data'=>$model)); ?>

<?php /*$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'system_id',
		'publisher',
		'genre',
		'release_date',
		'number_of_players',
		'cover_picture',
		'status',
		'submitter_id',
		'submit_date',
		'approve_date',
		'reject_date',
		'reject_comments',
	),
));   */?>