<?php
$this->breadcrumbs=array(
	'Queue',
);

?>

<h1>Copies of <?php echo $title->name; ?></h1>

<?php $this->renderPartial('_copies',array('dataProvider'=>$dataProvider)); ?>

