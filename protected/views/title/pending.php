<?php
$this->breadcrumbs=array(
	'Titles',
);

$this->menu=array(
	array('label'=>'Create Title', 'url'=>array('create')),
	array('label'=>'Manage Title', 'url'=>array('admin')),
);
?>

<h1>Titles</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'=>$dataProvider,
    'columns'=>array(
      'id',
      'name',
      'description',
      'system.name',
      'submitter.name',
      'submit_date',
array(
    'class'=>'CLinkColumn',
    'header'=>'Actions',
    'labelExpression'=>'"Update"',
    'urlExpression'=>'Yii::app()->createUrl("title/update",array("id"=>$data->id))'
),
    )
)); ?>
