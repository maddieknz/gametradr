<?php $this->widget('GTGridView', array(
	'dataProvider'=>$dataProvider,
    'rowClickUrlExpression'=>'Yii::app()->createUrl("asset/queue",array("id"=>$data->id))',
    'columns'=>array(
		array(
			'header'=>'Owner',
			'name'=>'owner.name',
		),
        array(
        	'class'=>'CLinkColumn',
        	'header'=>'Trade Status',
        	'labelExpression'=>'$data->lastHistory?$data->lastHistory->getStatusLabel():"Never Traded"',
           	'urlExpression'=>'Yii::app()->createUrl("asset/queue",array("id"=>$data->id))'	
        ),
        array(
        	'header'=>'Who Has It',
        	'value'=>'$data->getCurrentUser()->name'
        ),        
	)));
?>
