<?php
$this->breadcrumbs=array(
	'Titles'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Title', 'url'=>array('index')),
	array('label'=>'Manage Title', 'url'=>array('admin')),
);
?>

<h1>Create Title</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>