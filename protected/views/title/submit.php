<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'title-submit-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data')
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name'); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description'); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'publisher'); ?>
		<?php echo $form->textField($model,'publisher'); ?>
		<?php echo $form->error($model,'publisher'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'genre'); ?>
		<?php echo $form->textField($model,'genre'); ?>
		<?php echo $form->error($model,'genre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'release_date'); ?>
				<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
    			'name'=>'Title[release_date]',
    			// additional javascript options for the date picker plugin
    			'options'=>array(
        		'showAnim'=>'slide',
				'dateFormat'=>'yy-mm-dd',
				'changeMonth'=>'true',
				'changeYear'=>'true',
				
    			),
    			'htmlOptions'=>array(
        		'style'=>'height:20px;'
    			),
		));?>
		<?php echo $form->error($model,'release_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'number_of_players'); ?>
		<?php echo $form->textField($model,'number_of_players'); ?>
		<?php echo $form->error($model,'number_of_players'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cover_picture'); ?>		
		<?php echo $form->fileField($model,'cover_picture'); ?>
		<?php echo $form->error($model,'cover_picture'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'system_id'); ?>
		<?php echo $form->listBox($model,'system_id',System::selectList()); ?>
		<?php echo $form->error($model,'system_id'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->