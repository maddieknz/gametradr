<?php
$this->breadcrumbs=array(
	'Titles',
);

$this->menu=array(
	array('label'=>'Add A Title', 'url'=>array('create'))
);
?>

<h1>Titles</h1>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(	
	'id'=>'title-filter-form',
	'method'=>'GET',
	'enableAjaxValidation'=>false,
)); ?>


	<div class="row">
		<?php echo $form->labelEx($filterForm,'system_id'); ?>
		<?php echo $form->dropDownList($filterForm,'system_id',System::listData(),array('prompt'=>'[ALL]')); ?>
		<?php echo $form->error($filterForm,'system_id'); ?>
		</div>

	<div class="row">
		<?php echo $form->labelEx($filterForm,'genre_id'); ?>
		<?php echo $form->dropDownList($filterForm,'genre_id',Genre::listData(),array('prompt'=>'[ALL]')); ?>
		<?php echo $form->error($filterForm,'genre_id'); ?>
	</div>


<?php echo CHtml::submitButton('Search'); ?>

<?php $this->endWidget(); ?>

</div><!-- form -->

<ul class="title-list-view">

<?php $this->widget('zii.widgets.CListView', array(
    'dataProvider'=>$dataProvider,
    'itemView'=>'_view',   // refers to the partial view named '_view'
	'viewData'=>array('ownedTitles'=>$ownedTitles,'wishlistTitles'=>$wishlistTitles),
    'sortableAttributes'=>array(
        'name',
        'system_id',
        'genre'
        
    ),
)); ?>
</ul>

<script>
jQuery('.title-add-to-library').click(function() {
	var link = jQuery(this);
	jQuery.ajax(link.attr('href'),{
		data: {ajax:1},
		dataType: 'json',
		type: 'POST',
		success: function(data) {
			if(data.success)
				link.replaceWith('Added to Library');
			else
				alert('Title could not be added to your library.');
		},
		error: function() { alert('Title could not be added to your library.'); }	
	});
	return false;
});
jQuery('.title-add-to-wishlist').click(function() {
	var link = jQuery(this);
	jQuery.ajax(link.attr('href'),{
		data: {ajax:1},
		dataType: 'json',
		type: 'POST',
		success: function(data) {
			if(data.success)
				link.replaceWith('Added to Wishlist');
			else
				alert('Title could not be added to your wishlist.');
		},
		error: function() { alert('Title could not be added to your wishlist.'); }	
	});
	return false;
});
jQuery('.title-add-to > div').mouseleave(function() { jQuery(this).hide(); })
</script>

