<?php
$this->breadcrumbs=array(
	'Systems'=>array('index'),
	$model->name,
);
?>

<h1><?php echo $model->name; ?></h1>

<?php echo $this->renderPartial('_single', array('data'=>$model)); ?>

<?php /* $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'producer',
		'release_date',
		'picture',
	),
)); */ ?>
