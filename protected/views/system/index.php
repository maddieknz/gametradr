<?php
$this->breadcrumbs=array(
	'Systems',
);
?>

<h1>Systems</h1>
<ul class="list-view system-list">
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
</ul>

<style>
.system-list li { list-style: none; display: inline-block; width: 200px; height: 250px; margin: 15px 40px 0px 0px; text-align: center; vertical-align: top; }
.system-list .image { width: 200px; height: 200px; display: table-cell; vertical-align: middle; }
.system-list image { max-width: 200px; max-height: 200px; }
.system-list .name { font-size: 1.2em; }
</style>