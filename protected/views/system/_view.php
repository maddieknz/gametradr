<li>
	<?php $link = $this->createUrl('title/system',array('url_key'=>$data->url_key)); ?>
	
	<a href="<?php echo $link; ?>"><div class="name"><?php echo CHtml::encode($data->name); ?></div></a>
	<div class="image"><a href="<?php echo $link; ?>"><?php echo CHtml::image(CHtml::normalizeUrl(array('system/image','id'=>$data->id))); ?></a></div>
	<div class="developer"><?php echo CompanyHelper::link($data->developer); ?></div>
	<div class="release_date">Released <?php echo CHtml::encode($data->release_date); ?></div>
	

</li>