<?php
$this->breadcrumbs=array(
	'Friends',
);?>



	<h1>My Friends</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'=>$dataProvider,
    'columns'=>
	array(
      'second_user.name',
    array(
    'class'=>'CLinkColumn',
    'header'=>'Actions',
    'labelExpression'=>'"Remove"',
    'urlExpression'=>'Yii::app()->createUrl("friends/remove",array("link_id"=>$data->id))',
	),
		array(
    	'class'=>'CLinkColumn',
   		'header'=>'Actions',
    	'labelExpression'=>'"View Library"',
    	'urlExpression'=>'Yii::app()->createUrl("library/index",array("user_id"=>$data->second_user->id))'
	),

))); ?>

<h3>Send Friend Request</h3>

<?php echo $this->renderPartial('_request',array('username'=>'')); ?>

