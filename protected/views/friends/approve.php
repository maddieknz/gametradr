<?php
$this->breadcrumbs=array(
	'Friend Requests',
);?>


	<h1>Friend Requests</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider'=>new CArrayDataProvider($users),
    'columns'=>array(
    array(
        'class'=>'CLinkColumn',
        'header'=>'From User',
        'labelExpression'=>'$data->username',
        'urlExpression'=>"Yii::app()->createUrl('user/view',array('username'=>\$data->username))",
    ),
    array(
        'class'=>'CLinkColumn',
        'header'=>'Approve',
        'label'=>'Approve',
        'urlExpression'=>"Yii::app()->createUrl('friends/approve',array('username'=>\$data->username,'status'=>'approve'))",
    ),
    array(
        'class'=>'CLinkColumn',
        'header'=>'Reject',
        'label'=>'Reject',
        'urlExpression'=>"Yii::app()->createUrl('friends/approve',array('username'=>\$data->username,'status'=>'reject'))",
    )))); ?>


