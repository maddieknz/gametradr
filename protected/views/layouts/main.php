<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<!-- blueprint CSS framework -->
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/gt.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
	<div class="container">
	<div class="row pomegranate">
	<!-- header -->
		<div class="span4">
			<div id="logo"><h1>GAMETRADR</h1></div>
		</div>
		<!-- mainmenu -->
		<div class="span5 offset3">
			<div class="user-menu">
				<?php $this->widget('zii.widgets.CMenu',array(
					'items'=>array(
					array('label'=>'Library', 'url'=>array('/library/index'),'visible'=>!Yii::app()->user->isGuest),
					array('label'=>'Wish List', 'url'=>array('/wishlist/index'),'visible'=>!Yii::app()->user->isGuest),
					array('label'=>'Trades', 'url'=>array('/asset/user'),'visible'=>!Yii::app()->user->isGuest),
					array('label'=>'Invite', 'url'=>array('/user/invite'),'visible'=>!Yii::app()->user->isGuest),
					array('label'=>'Friends', 'url'=>array('/friends/index'),'visible'=>!Yii::app()->user->isGuest),
				        array('label'=>'Notifications'.
				(User::current() ? ('(' . count(User::current()->getNotifications()) . ')') : ''), 'url'=>array('/user/notifications'),'visible'=>!Yii::app()->user->isGuest),
					array('label'=>'Admin', 'url'=>array('/admin/dashboard'), 'visible'=>User::admin()),
					array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
					array('label'=>'Logout', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
					),
				)); ?>
			</div>
		</div>
	</div>
	<div class="row white">
			<div class="navbar">
				<div class="navbar-inner">
					
	<?php
	$items = array(array('label'=>'Home', 'url'=>array('/site/index')));
	foreach(System::model()->findAll(array('condition'=>'featured_sort_order is not null and url_key is not null','order'=>'featured_sort_order')) as $system)
		$items[] = array('label'=>$system->name, 'url'=>array('title/system','url_key'=>$system->url_key));
	$items[] = array('label'=>'All Systems', 'url'=>array('/system/index')); ?>
						
	<?php $this->widget('zii.widgets.CMenu',array('items'=>$items,'htmlOptions'=>array('class'=>'nav'))); ?>
	</div>
</div>
</div>

	<?php $flashes = Yii::app()->user->getFlashes(true); ?>
	<?php if(!empty($flashes)): ?>
		<ul class="flashes">
			<?php foreach($flashes as $type=>$message): ?>
				<li class="<?php echo $type; ?>"><?php echo CHtml::encode($message); ?></li>
			<?php endforeach; ?>
		</ul>
	<?php endif; ?>

	<div class="row white">
		<div class="span12">
			<?php echo $content; ?>
		</div>
	</div>
	<hr>
	<!-- footer -->
	<div class="row pomegranate">
		<footer>
			<div class="span4">
		<p>&copy; <?php echo date('Y'); ?> GameTradr.
		All Rights Reserved.</p>
			</div>
			<div class="span4 offset4">
				<div class="footer-menu">
					<?php $this->widget('zii.widgets.CMenu',array(
						'items'=>array(
						array('label'=>'About', 'url'=>array('/site/about', 'view'=>'about')),
						array('label'=>'Contact', 'url'=>array('/site/contact')),

						),
					)); ?>
			</div>
		</div>
		</footer>
	</div>
</div>
</div>
</body>
</html>
