<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<!-- blueprint CSS framework -->
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
	<div class="container">
	<div class="row pomegranate">
	<!-- header -->
		<div class="span4">
			<div id="logo"><h1>GT Admin</h1></div>
		</div>
		<!-- mainmenu -->
		<div class="span5 offset3">
			<div class="user-menu">
				<?php $this->widget('zii.widgets.CMenu',array(
					'items'=>array(
					array('label'=>'Library', 'url'=>array('/library/index')),
					array('label'=>'Wish List', 'url'=>array('/wishlist/index')),
					array('label'=>'Trades', 'url'=>array('/asset/user')),
					array('label'=>'Invite', 'url'=>array('/user/invite')),
					array('label'=>'Friends', 'url'=>array('/friends/index')),				
				        array('label'=>'Notifications'.
				(User::current() ? ('(' . count(User::current()->getNotifications()) . ')') : ''), 'url'=>array('/user/notifications')),
					array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
					array('label'=>'Logout', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
					),
				)); ?>
			</div>
		</div>
	</div>
	<div class="row white">
			<div class="navbar">
				<div class="navbar-inner">
					<?php $this->widget('zii.widgets.CMenu',array(
					'items'=>array(
					array('label'=>'Home', 'url'=>array('/site/index')),
					array('label'=>'Titles', 'url'=>array('/admin/title/index')),
					array('label'=>'Systems', 'url'=>array('/admin/system/index')),
					array('label'=>'Companies', 'url'=>array('/admin/company/index')),		
					array('label'=>'Genres', 'url'=>array('/admin/genre/index')),	
					array('label'=>'Users', 'url'=>array('/admin/user/index')))
,'htmlOptions'=>array('class'=>'nav')
				)); ?>
	</div>
</div>
</div>
	<div class="row white">
		<div class="span9">
			<?php echo $content; ?>
		</div>
		<div class="span3">
	<?php
			$this->beginWidget('zii.widgets.CPortlet', array(
				'title'=>'Operations','htmlOptions'=>array('class'=>'nav-header')
			));
			$this->widget('zii.widgets.CMenu', array(
				'items'=>$this->menu,
				'htmlOptions'=>array('class'=>'nav nav-tabs nav-stacked'),
			));
			$this->endWidget();
		?>
	</div>
	</div>
	<!-- footer -->
	<div class="row pomegranate">
		<footer>
			<div class="span4">
		<p>&copy; <?php echo date('Y'); ?> GameTradr.
		All Rights Reserved.</p>
			</div>
			<div class="span4 offset4">
				<div class="footer-menu">
					<?php $this->widget('zii.widgets.CMenu',array(
						'items'=>array(
						array('label'=>'About', 'url'=>array('/site/about', 'view'=>'about')),
						array('label'=>'Contact', 'url'=>array('/site/contact')),

						),
					)); ?>
			</div>
		</div>
		</footer>
	</div>
	</div>

</div><!-- page -->

</body>
</html>
