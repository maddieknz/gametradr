<?php

class StreamController extends Controller {
  
  public function actionIndex() {
    $dataProvider = new CActiveDataProvider('Stream',array(
        'sort'=>array(
            'defaultOrder' => 'id desc'
        )
    ));
    $this->render('index',array('dataProvider'=>$dataProvider));
  }
  
  public function actionView($id) {
    $model = Stream::model()->findByPk($id);
    if(!$model || !$model->isVisible()) $this->fail404();
    $this->render('view',array('model'=>$model));
  }
}

?>