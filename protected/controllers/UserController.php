<?php

class UserController extends Controller
{
	public function actionView()
	{
		$model = null;
		if(isset($_GET['id']))
			$model = User::model()->findByPk($_GET['id']);
		else if(isset($_GET['username']))
			$model = User::model()->find('username=:username',array(':username'=>$_GET['username']));
		if(!$model) $this->fail404();


		$this->render('view',array(
			'model'=>$model
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new User;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionNotifications()
	{
		$this->render('notifications',array('user'=>User::current()));
	}
	
	public function actionInvite()
	{
		if (isset($_POST['email'])){
			$user=$this->loadModel(Yii::app()->user->id);
			if($user->invites < 1){
				die('No invites');
			}
			else{
/*			    if(UserInvite::model()->count('email=:email and user_id=:user_id',
			      array( ':email' =>$_POST['email'], ':user_id' => Yii::app()->user->id)) > 0 ) {
                    die("already invited");			        
			      }*/
			      
				$invite=new UserInvite();
				$invite->email= $_POST['email'];
				$invite->user_id=Yii::app()->user->id;
				$invite->generateRandom();
				$invite->save();
				
				$user->invites--;
				$user->save();
				
				$message = new YiiMailMessage;
				$url = $this->createAbsoluteUrl("user/register",array('random'=>$invite->random));
 				$message->setBody('<p>Hello, <br /> To register for Game Traitors please use this link: <a href="'.$url.'">'.$url.'</a>', 'text/html');
 				$message->subject = 'Register for Game Traitors!';
 				$message->addTo($_POST['email']);
 				$message->from = Yii::app()->params['adminEmail'];
 				if(!Yii::app()->mail->send($message))
				  die('Mail Failure');
				
				
				$this->redirect('index');
			}
		}
		$this->render('invite');
	}
	
	public function actionRegister($random)
	{
		$invite = UserInvite::model()->find(array(
        	'condition'=>'random=:random',            
            'params'=>array(':random'=>$random)));    
		if(!$invite){
			die('Invalid invite code');
		}
		$model=new User('register');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			$model->refer_user_id=$invite->user_id;
						
			if($model->save()){
				// Generate the stream message
				$stream = new Stream;
				$stream->type = 'NewUser';
				$stream->visibility =  Stream::VISIBILITY_GLOBAL;
				$stream->user_id = $model->id;
				$stream->save;

				$invite->delete();
				$this->redirect(array('view','id'=>$model->id));
			}
				
		}

		$this->render('register',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=User::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
		public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',
				'users'=>array('@'),
			),
			array('allow',
				'actions'=>array('view')  
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

}
