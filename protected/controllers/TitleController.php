<?php

class TitleController extends Controller
{
  /**
   * Displays a particular model.
   * @param integer $id the ID of the model to be displayed
   */
  public function actionView($id=null,$url_key=null)
  {
    $this->render('view',array(
			'model'=>$this->loadModel($id,$url_key),
    ));
  }
    
  	public function actionCopies($id)
	{
		$model = $this->loadModel($id);
				  
	    $dataProvider=new CActiveDataProvider('Asset', array(
      		'criteria' => array(
				'condition'=>'title_id=:title',
				'params'=>array(':title'=>$model->id)
			)
        ));
		
		$this->render('copies',array('title'=>$model,'dataProvider'=>$dataProvider));
	}

   public function actionCoverPicture($id) {

	  $model = $this->loadModel($id);
	  
	  $media = Media::model()->find('title_id=:title and type=:type and status=:status',
	    array(':title'=>$model->id,':type'=>Media::TYPE_TITLE_COVER,':status'=>Media::STATUS_DEFAULT));
	    
	  if(!$media)
	    throw new CHttpException(404); // TODO: no system image	  

	  $media->sendToBrowser();
	}
  
  /**
   * Creates a new model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   */
  public function actionCreate()
  {
    $model=new Title;

    // Uncomment the following line if AJAX validation is needed
    // $this->performAjaxValidation($model);

    if(isset($_POST['Title']))
    {
      $model->attributes=$_POST['Title'];
      if($file = CUploadedFile::getInstance($model, 'cover_picture'))
   	  $model->cover_picture=$file;
   	  $model->submitter_id = Yii::app()->user->id;
	  $model->status = Title::STATUS_SUBMITTED;
	  
      if($model->save())
      $this->redirect(array('view','id'=>$model->id));
    }

    $this->render('create',array(
			'model'=>$model,
    ));
  }

  /**
   * Lists all models.
   */
  public function actionIndex()
  {
    $this->_actionIndex();
  }

  public function actionSystem($url_key)
  {
    $filterForm = new TitleIndexFilter;
    $system = System::model()->findByUrlKey($url_key);
    if(!$system)
	    throw new CHttpException(404);
    $filterForm->system_id = $system->id;
    $this->_actionIndex($filterForm);
  }
  
  private function _actionIndex($filterForm = null)
  {
    if(!$filterForm)
      $filterForm = new TitleIndexFilter;
    $dataProvider=new GTActiveDataProvider('Title');
    if(isset($_GET['TitleIndexFilter']))
       $filterForm->attributes = $_GET['TitleIndexFilter'];
    if($filterForm->validate())
       $filterForm->apply($dataProvider->criteria);

    $this->render('index',array(
			'filterForm'=>$filterForm,
			'dataProvider'=>$dataProvider,
			'ownedTitles'=>UserHelper::findTitlesInLibrary($dataProvider->getData()),
			'wishlistTitles'=>UserHelper::findTitlesInWishlist($dataProvider->getData())
    ));
  }  
  
  public function actionPending()
  {
    $dataProvider=new CActiveDataProvider('Title', array(
      'criteria' => array(
        'condition'=>'status='.Title::STATUS_SUBMITTED
      ))
    );
    $this->render('pending',array(
			'dataProvider'=>$dataProvider,
    ));
  }

  /**
   * Returns the data model based on the primary key given in the GET variable.
   * If the data model is not found, an HTTP exception will be raised.
   * @param integer the ID of the model to be loaded
   */
  public function loadModel($id=null,$url_key=null)
  {
    $model = null;
    if($id)
        $model=Title::model()->findByPk((int)$id);
    else if($url_key)
        $model=Title::model()->findByUrlKey($url_key);     
    if($model===null)
    throw new CHttpException(404,'The requested page does not exist.');
    return $model;
  }

  /**
   * Performs the AJAX validation.
   * @param CModel the model to be validated
   */
  protected function performAjaxValidation($model)
  {
    if(isset($_POST['ajax']) && $_POST['ajax']==='title-form')
    {
      echo CActiveForm::validate($model);
      Yii::app()->end();
    }
  }
  
  	public function accessRules()
	{
		return array(
			array('allow',
				'users'=>array('@'),
			),
			array('allow',
				'actions'=>array('index,coverPicture'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
}
