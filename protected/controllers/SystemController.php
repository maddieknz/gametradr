<?php

class SystemController extends Controller {

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionImage($id) {
	  
	  $model = $this->loadModel($id);
	  $media = Media::model()->find('system_id=:system and type=:type and status=:status',
	    array(':system'=>$model->id,':type'=>Media::TYPE_SYSTEM_IMAGE,':status'=>Media::STATUS_DEFAULT));
	    
	  if(!$media)
	    throw new CHttpException(404); // TODO: no system image	  

	  $media->sendToBrowser();
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('System');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=System::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='system-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	

}
