<?php

class FriendsController extends Controller
{
	
	public function actionRequest($username = null) {
		if($username) {
			$user = User::model()->findByUsername($username);
			if(!$user)
				Yii::app()->user->setFlash('error',"The username '$username' was not found.");
			else {
				list($result,$msg) = UserHelper::processFriendRequest(User::current(),$user);
								
				Yii::app()->user->setFlash($result,$msg);
				if($result == 'success') $username = '';
			}
		}

		$this->render('request',array('username'=>$username));		
	}
	
	public function actionApprove($username = null,$status = null) {
		
		if($username && $status) {
			$user = User::model()->findByUsername($username);
			if(!$user)
				Yii::app()->user->setFlash('error',"The username '$username' was not found.");
			else {
				list($result,$msg) = UserHelper::processFriendRequestResponse(User::current(),$user,$status);
								
				Yii::app()->user->setFlash($result,$msg);
				$this->redirect(array('approve'));
			}
		}		
		
		$users = User::model()->findAll(array(
			'condition'=>'l.status=:status and l.link_type=:type and l.second_user_id=:user',
			'join'=>'inner join user_link l on t.id=l.first_user_id',
			'params'=>array(':status'=>UserLink::STATUS_REQUESTED,':type'=>UserLink::TYPE_FRIEND,':user'=>Yii::app()->user->id)));

		$this->render('approve',array('users'=>$users));		
	}
	
  public function loadUserModel($id)
  {
    $model=User::model()->findByPk((int)$id);
    if($model===null)
    throw new CHttpException(404,'The requested page does not exist.');
    return $model;
  }
	

	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('UserLink', array(
      	'criteria' => array(
          'condition'=>'first_user_id=:user and status=:status and link_type=:type',
	  'params'=>array(':user'=>Yii::app()->user->id,':status'=>UserLink::STATUS_CONFIRMED,':type'=>UserLink::TYPE_FRIEND))));

         $this->render('index',array(
			'dataProvider'=>$dataProvider,
      ));
        
        
	}

	public function actionRemove($link_id)
	{
		$user_link = $this->loadUserLinkModel($link_id)->delete();
	    
	    $this->redirect('index');
	}
  public function loadUserLinkModel($id)
  {
    $model=UserLink::model()->findByPk((int)$id);
    if($model===null)
    throw new CHttpException(404,'The requested page does not exist.');
    return $model;
  }
	
	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

}