<?php

class CompanyController extends Controller {
    
    public function actionLogo($id) {

	  $model = Company::model()->findByPk($id);
	  
	  if(!$model)
	    throw new CHttpException(404); // TODO: no system image	  
          
	  $media = Media::model()->find('company_id=:company and type=:type and status=:status',
	    array(':company'=>$model->id,':type'=>Media::TYPE_COMPANY_LOGO,':status'=>Media::STATUS_DEFAULT));
	    
	  if(!$media)
	    throw new CHttpException(404); // TODO: no system image	  

	  $media->sendToBrowser();
    }
    
    public function actionLargeImage($id) {

	  $model = Company::model()->findByPk($id);
	  
	  if(!$model)
	    throw new CHttpException(404); // TODO: no system image	  
          
	  $media = Media::model()->find('company_id=:company and type=:type and status=:status',
	    array(':company'=>$model->id,':type'=>Media::TYPE_COMPANY_LARGE_IMAGE,':status'=>Media::STATUS_DEFAULT));
	    
	  if(!$media)
	    throw new CHttpException(404); // TODO: no system image	  

	  $media->sendToBrowser();
    }
}
