<?php

class WishlistController extends Controller
{
    public function loadUserWishlist() {
        return UserWishlist::model()->findAll(array(
        	'condition'=>'user_id=:user_id',
            'order'=>'priority',
            'params'=>array(':user_id'=>Yii::app()->user->id)));      
    }
    
    public function loadQueueModelByTitle($title_id) {
        $model=UserWishlist::model()->find(array(
             'condition'=>'user_id=:user_id and title_id=:title_id',
            'params'=>array(":user_id"=>Yii::app()->user->id,':title_id'=>$title_id)));
        if($model===null)
        throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
  
	public function actionAdd($title_id)
	{        
	    $queue = $this->loadUserWishlist();
	  
	    $model = new UserWishlist();
	    $model->user_id = Yii::app()->user->id;
	    $model->title_id = $title_id;	    
        if(empty($queue)) {
            $model->priority = 1;
        } else {
            end($queue);
            $model->priority=current($queue)->priority+1;          
        }

		if(isset($_REQUEST['ajax'])) {
			echo CJSON::encode(array('success'=>$model->save()));
			die();
		}
		
        if($model->save())
          Yii::app()->user->setFlash('queue-add','The title has been added to your wishlist.');
        else
          Yii::app()->user->setFlash('queue-add','The title could not be added to your wishlist.');
	  
		$this->redirect('index');
	}

	public function actionIndex()
	{
		$user = null;
		if(isset($_GET['user_id']))
			$user = User::model()->findByPk($_GET['user_id']);
		else if(isset($_GET['username']))
			$user = User::model()->findByUsername($_GET['username']);
		else if(Yii::app()->user->isGuest)
			Yii::app()->user->loginRequired();
		else
			$user = User::model()->findByPk(Yii::app()->user->id);
		if(!$user)
			throw new CHttpException(404);
			
	    if(isset($_POST['priority']) && Yii::app()->user->id == $user->id)
            $this->updatePriorities($_POST['priority']);          
	  
	    $dataProvider=new CActiveDataProvider('UserWishlist', array(
      		'criteria' => array(
          		'condition'=>'user_id='.$user->id ,
	            'order'=>'priority' 
          ))
        );
              
        $this->render('index',array(
			'user'=>$user,
			'dataProvider'=>$dataProvider,
        ));      
	}

	public function updatePriorities($map) {
	    $queue = $this->loadUserWishlist();
	    
	    $reordered = array();
	    
	    foreach($queue as $model) {
          if(!isset($map[$model->id]) || !$map[$model->id]) {
            $model->delete();
            continue;            
          }
	      
          $index = (int) $map[$model->id];
          if(!isset($reordered[$index]))
              $reordered[$index] = array();

          $reordered[$index][] = $model;
	    }
	    
	    ksort($reordered);
	    $priority = 0;
	    
        foreach($reordered as $array) {
          foreach($array as $model) {
             $priority++;
             if($model->priority != $priority) {
               $model->priority = $priority;
               $model->save();
             }            
          }
        }	    	   	  
	}
	
	public function actionRemove($title_id)
	{
	    $asset = $this->loadQueueModelByTitle($title_id);
		if($asset->user_id == Yii::app()->user_id)
			$asset->delete();
	    
		$this->redirect('index',array('username'=>$asset->user->username));
	}

 	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',
				'users'=>array('@'),
			),
			array('allow',
				'actions'=>array('index'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
}