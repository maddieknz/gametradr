<?php

class TradeController extends Controller
{
	public function actionIndex()
	{
       $user_id = Yii::app()->user->id;
       
       $model = Trade::model();
       
	   $this->render('index',array(
	     'gamesToSend' => $model->findGamesToSend($user_id),
	     'gamesToAck'  => $model->findGamesToAck($user_id),
	     'gamesReceived' => $model->findGamesReceived($user_id)
	   ));
	}
	
	public function actionUpdate($trade_id)
	{
       $user_id = Yii::app()->user->id;
	  
       $trade = $this->loadTrade($trade_id);

       switch($trade->status) {
         case Trade::STATUS_WAITING_FOR_MAILING:
            if(isset($_POST['Trade']['mail_date'])) {
              $trade->mail_date = $_POST['Trade']['mail_date'];
              $trade->status = Trade::STATUS_IN_TRANSIT;
              if($trade->save())
                $this->redirect(array('trade/index')); 
            } else
              $trade->mail_date = strftime('%Y-%m-%d');
            $this->render('update_mail',array('trade'=>$trade));
            break;         
         case Trade::STATUS_IN_TRANSIT:
            if(isset($_POST['Trade']['receive_date'])) {
              $trade->receive_date = $_POST['Trade']['receive_date'];
              $trade->status = Trade::STATUS_RECEIVED;
              if($trade->save())
                $this->redirect(array('trade/index')); 
            } else
              $trade->receive_date = strftime('%Y-%m-%d');
            $this->render('update_transit',array('trade'=>$trade));
            break;         
         case Trade::STATUS_RECEIVED:
            if(isset($_POST['ready'])) {
              $trade->ready_date = strftime('%Y-%m-%d');
              $trade->status = Trade::STATUS_READY_FOR_NEXT_MAILING;
              if($trade->save())
                $this->redirect(array('trade/index')); 
              
            }
            $this->render('update_received',array('trade'=>$trade));
            break;               
         default:
             throw new CHttpException(404,'The requested page does not exist.');         
       }       
	}

    public function loadTrade($id) {
      $model=Trade::model()->findByPk((int)$id);
      if($model===null)
      throw new CHttpException(404,'The requested page does not exist.');
      return $model;
	}
	
  /**
   * @return array action filters
   */
  public function filters()
  {
    return array(
			'accessControl', // perform access control for CRUD operations
    );
  }

  /**
   * Specifies the access control rules.
   * This method is used by the 'accessControl' filter.
   * @return array access control rules
   */
  public function accessRules()
  {
    return array(
    array('allow',
    			'actions'=>array('index','update'),
    			'users'=>array('@'),
    ),
        array('deny',  // deny all users
				'users'=>array('*'),
    ),
    );
  }
}