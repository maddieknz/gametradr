<?php

class AssetController extends Controller {
  
  public function actionQueue($id) {
        $asset = Asset::model()->findByPk($id);
        if(!$asset || $asset->isVisible() == false) throw new CHttpException(404);
        
        $this->render('queue',array('asset'=>$asset));        
  }
  
  public function actionUser() {
    $dataProvider = new CActiveDataProvider('AssetQueue',
      array('criteria'=>array(
        'condition'=>'user_id=:user',
        'params'=>array(':user'=>Yii::app()->user->id)
      )
    ));
    
    $this->render('user',array('dataProvider'=>$dataProvider));
  }

  public function actionShip($id) {
    $asset = Asset::model()->findByPk($id);
  
    // Validate model 
    if(!$asset
    || !$asset->lastHistory
    || $asset->lastHistory->action != AssetQueue::STATUS_WAITING_FOR_SHIP
    || $asset->lastHistory->user_id != Yii::app()->user->id) throw new CHttpException(404);

    if(isset($_POST['submit'])) {
      if($asset->updateStatus(AssetQueue::STATUS_IN_TRANSIT)) {
        $this->render('ship_success',array('asset'=>$asset));
	return;
      	}
    }

    $this->render('ship',array('asset'=>$asset));
  }

  public function actionReceive($id) {
    $asset = Asset::model()->findByPk($id);

    // Validate model 
    if(!$asset
    || !$asset->lastHistory
    || $asset->lastHistory->action != AssetQueue::STATUS_IN_TRANSIT
    || $asset->lastHistory->user_id != Yii::app()->user->id) throw new CHttpException(404);

    if(isset($_POST['submit'])) {
      if($asset->updateStatus($asset->lastHistory->queue_id ? AssetQueue::STATUS_PLAYING : AssetQueue::STATUS_COMPLETE)) {
        $this->render('receive_success',array('asset'=>$asset));
	return;
      	}
    }

    $this->render('receive',array('asset'=>$asset));
  }
  
  public function actionComplete($id) {
    $asset = Asset::model()->findByPk($id);

    // Validate model 
    if(!$asset
    || !$asset->lastHistory
    || $asset->lastHistory->action != AssetQueue::STATUS_PLAYING
    || $asset->lastHistory->user_id != Yii::app()->user->id) throw new CHttpException(404);

    if(isset($_POST['submit'])) {
      if($asset->updateStatus(AssetQueue::STATUS_COMPLETE)) {
        $asset->updateHistory();
        $this->render('complete_success',array('asset'=>$asset));
    	return;
      	}
    }

    $this->render('complete',array('asset'=>$asset));
  }
  
  public function actionApprove() {
        if(isset($_GET['id'])) {
            $model = AssetRequest::model()->findByPk($_GET['id']);
            if(!$model || $model->asset->owner_id != Yii::app()->user->id) throw new CHttpException(404);
            $model->status = $_GET['status'];
            if($model->save()) {
                if($model->status == AssetRequest::STATUS_ACCEPTED) {
                    $model->convertToQueue();
                }                        
            }
        }
    
        $dataProvider = new CActiveDataProvider('AssetRequest',array('criteria'=>array(
            'condition'=>'status=:status and asset.owner_id=:owner',
            'with'=>'asset',
            'params'=>array('status'=>AssetRequest::STATUS_REQUESTED,':owner'=>Yii::app()->user->id)
            )));
        
        $this->render('approve',array('dataProvider'=>$dataProvider));    
  }
  
  public function actionRequest($id) {
        $asset = Asset::model()->findByPk($id);
        if(!$asset || $asset->isVisible() == false) throw new CHttpException(404);
        
        // Look for an existing request
        $model = AssetRequest::model()->find('asset_id=:asset and user_id=:user and status=:status',array(
            ':asset'=>$asset->id,
            ':user'=>Yii::app()->user->id,
            ':status'=>AssetRequest::STATUS_REQUESTED
        ));
        
        if(!$model) {
            $model = new AssetRequest;
            $model->asset_id = $asset->id;
            $model->user_id = Yii::app()->user->id;
            $model->status = AssetRequest::STATUS_REQUESTED;
            $model->request_date = date('Y-m-d h:i:s');
        }
        
        if(isset($_POST['AssetRequest'])) {
            $model->attributes = $_POST['AssetRequest'];
            if($model->save())
                $this->redirect(array('queue','id'=>$asset->id));
        }
        $this->render('request',array('asset'=>$asset,'model'=>$model));            
  }
  
  	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
}

?>