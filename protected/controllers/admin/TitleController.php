<?php

class TitleController extends AdminController
{

  /**
   * Displays a particular model.
   * @param integer $id the ID of the model to be displayed
   */
  public function actionView($id)
  {
	$model = $this->loadModel($id);
	
    $this->render('view',array(
			'model'=>$this->loadModel($id),
    ));
  }
  
  public function actionSubmit() {
    $model=new Title('submit');

    // uncomment the following code to enable ajax-based validation
    /*
    if(isset($_POST['ajax']) && $_POST['ajax']==='title-submit-form')
    {
    echo CActiveForm::validate($model);
    Yii::app()->end();
    }
    */

    if(isset($_POST['Title']))
    {                  
   	  $model->attributes=$_POST['Title'];
      
      if($file = CUploadedFile::getInstance($model, 'cover_picture'))
   			$model->cover_picture=$file;
   	  $model->submitter_id = Yii::app()->user->id;		
   	  $model->status = Title::STATUS_SUBMITTED;
   	  $model->submit_date = getdate();
   	  

   	  if($model->save())
   			$this->redirect(array('index'));
    }
    $this->render('submit',array('model'=>$model));
  }
  
  /**
   * Creates a new model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   */
  public function actionCreate()
  {
    $model=new Title;
	$model->status = Title::STATUS_ACTIVE;
    $model->submitter_id = Yii::app()->user->id;

    // Uncomment the following line if AJAX validation is needed
    // $this->performAjaxValidation($model);

    if(isset($_POST['Title']))
    {
      $model->attributes=$_POST['Title'];
      if($file = CUploadedFile::getInstance($model, 'cover_picture'))
     	  $model->cover_picture=$file;
	  
      if($model->save()) {
		if($model->status == Title::STATUS_ACTIVE) {
		  $streamId = Yii::app()->user->getState('NewTitleStreamID');
		  $stream = $streamId ? Stream::model()->findByPk($streamId) : null;
		  if(!$stream) {
			$stream = new Stream;
			$stream->visibility = Stream::VISIBILITY_GLOBAL;
			$stream->type = 'NewTitle';
			$stream->user_id = Yii::app()->user->id;			
		  }
		  $stream->typeModel->addTitle($model);
		  if(!$stream->save()) {
			var_dump($stream->errors);
			die();
		  }
		  Yii::app()->user->setState('NewTitleStreamID',$stream->id);
		}
        $this->redirect(array('index'));
      }
    }

    $this->render('create',array(
			'model'=>$model,
    ));
  }

  /**
   * Updates a particular model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id the ID of the model to be updated
   */
  public function actionUpdate($id)
  {
    $model=$this->loadModel($id);

    // Uncomment the following line if AJAX validation is needed
    // $this->performAjaxValidation($model);

    if(isset($_POST['Title']))
    {
      $model->attributes=$_POST['Title'];
	  
      if($file = CUploadedFile::getInstance($model, 'cover_picture'))
   	  $model->cover_picture=$file;
      if($model->save())
      $this->redirect(array('index'));
    }

    $this->render('update',array(
			'model'=>$model,
    ));
  }

  /**
   * Deletes a particular model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id the ID of the model to be deleted
   */
  public function actionDelete($id)
  {
    if(Yii::app()->request->isPostRequest)
    {
      // we only allow deletion via POST request
      $this->loadModel($id)->delete();

      // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
      if(!isset($_GET['ajax']))
      $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }
    else
    throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
  }

  /**
   * Lists all models.
   */
  public function actionIndex()
  {
    $model=new Title('search');
    $model->unsetAttributes();  // clear any default values
    if(isset($_GET['Title']))
    $model->attributes=$_GET['Title'];

    $this->render('index',array(
			'model'=>$model,
    ));
  }

  public function actionPending()
  {
    $dataProvider=new CActiveDataProvider('Title', array(
      'criteria' => array(
        'condition'=>'status='.Title::STATUS_SUBMITTED
      ))
    );
    $this->render('pending',array(
			'dataProvider'=>$dataProvider,
    ));
  }

  /**
   * Returns the data model based on the primary key given in the GET variable.
   * If the data model is not found, an HTTP exception will be raised.
   * @param integer the ID of the model to be loaded
   */
  public function loadModel($id)
  {
    $model=Title::model()->findByPk((int)$id);
    if($model===null)
    throw new CHttpException(404,'The requested page does not exist.');
    return $model;
  }

  /**
   * Performs the AJAX validation.
   * @param CModel the model to be validated
   */
  protected function performAjaxValidation($model)
  {
    if(isset($_POST['ajax']) && $_POST['ajax']==='title-form')
    {
      echo CActiveForm::validate($model);
      Yii::app()->end();
    }
  }
}
