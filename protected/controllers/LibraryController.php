<?php

class LibraryController extends Controller
{
	public function actionAdd($title_id)
	{
	    $title = $this->loadTitleModel($title_id);
	    
	    $asset = new Asset();
	    $asset->owner_id = Yii::app()->user->id;
	    $asset->title_id = $title->id;
	    $asset->available = 1;
	    $asset->trade_policy = 0;
	    $asset->visibility = 0;
	    
		if($success = $asset->save()) {
			$streamId = Yii::app()->user->getState('AddToLibraryStreamID');
			$stream = $streamId ? Stream::model()->findByPk($streamId) : null;
			if(!$stream) {
				$stream = new Stream;
				$stream->visibility = Stream::VISIBILITY_FRIENDS;
				$stream->type = 'AddToLibrary';
				$stream->user_id = Yii::app()->user->id;			
			}
			$stream->typeModel->addTitle($title);
			$stream->save();
			Yii::app()->user->setState('AddToLibraryStreamID',$stream->id);
		}
		
		if(isset($_REQUEST['ajax'])) {
			echo CJSON::encode(array('success'=>$success));
			die();
		}
		
        if($success)
          Yii::app()->user->setFlash('library-add','The title has been added to your library.');
        else
          Yii::app()->user->setFlash('library-add','The title could not be added to your library.');
        
        $this->redirect('index');
	}
	
	public function actionAjaxChange($asset_id)
	{
       	 $asset = $this->loadAssetModel($asset_id);
       	 
    	if(isset($_POST['Asset']))
		{
		    $asset->attributes=$_POST['Asset'];
    			
			if($asset->save())
			  return;
		}
       	 
       	 $this->renderPartial('ajax/change',array('asset'=>$asset));	  
	}

	public function actionIndex()
	{
		$user = null;
		if(isset($_GET['user_id']))
			$user = User::model()->findByPk($_GET['user_id']);
		else if(isset($_GET['username']))
			$user = User::model()->findByUsername($_GET['username']);
		else if(Yii::app()->user->isGuest)
			Yii::app()->user->loginRequired();
		else
			$user = User::model()->findByPk(Yii::app()->user->id);
		if(!$user)
			throw new CHttpException(404);
			
      $dataProvider=new CActiveDataProvider('Asset', array(
      	'criteria' => array(
          'condition'=>'owner_id=:user',
		  'params'=>array(':user'=>$user->id)
        )));
      
       Asset::applyVisibilityCriteriaForLibrary($user,$dataProvider->criteria);

       // At this time we are updating asset status here, just in case
       foreach($dataProvider->getData() as $asset)
         $asset->updateHistory();
	  
      $this->createWidget('GTJuiDialog');
      
      $this->render('index',array(
			'dataProvider'=>$dataProvider,
			'user'=>$user
      ));
      
	}

	public function actionRemove($asset_id)
	{
	    $asset = $this->loadAssetModel($asset_id)->delete();
	    
	    $this->redirect('index');
	}
	
  public function loadTitleModel($id)
  {
    $model=Title::model()->findByPk((int)$id);
    if($model===null)
    throw new CHttpException(404,'The requested page does not exist.');
    return $model;
  }
  
  public function loadAssetModel($id)
  {
    $model=Asset::model()->findByPk((int)$id);
    if($model===null)
    throw new CHttpException(404,'The requested page does not exist.');
    return $model;
  }
  
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('add','remove','ajaxChange'),
				'users'=>array('@'),
			),
			array('allow',
				'actions'=>array('index')  
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
}